<?php 
    include('../config/koneksi.php');
            
    // menampilkan data pegawai
    $data = mysqli_query($koneksi,"SELECT * FROM profil");

    $home = mysqli_fetch_array($data);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Export Data Pegawai</title>
</head>
<body>
	<style type="text/css">
	body{
		font-family: sans-serif;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table#data th,
	table#data td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;

	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>

	<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Export-Data-Pegawai-$_GET[jenis_pegawai]-".date('Y-m-d').".xls");
	?>

	<table border="0">
    <tr>
        <td></td>
        <td></td>
        <td>
            <h3>Export Data Pegawai <?=$home['nama']?></h3>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <h3>Tanggal <?=date('Y-m-d')?></h3>
        </td>

    </tr>
    <tr></tr>
    <tr></tr>
    </table>
	

	<table border="1" id="data">
		<tr>
			<th>No</th>
            <?php if (isset($_GET['nip'])) {?>
                <th>NIP</th>
            <?php } ?>
            <?php if (isset($_GET['nama'])) {?>
                <th>Nama</th>
            <?php } ?>
            <?php if (isset($_GET['nik'])) {?>
                <th>NIK</th>
            <?php } ?>
            <?php if (isset($_GET['kelamin'])) {?>
                <th>Jenis Kelamin</th>
            <?php } ?>
            <?php if (isset($_GET['tempat_lahir'])) {?>
                <th>Tempat Lahir</th>
            <?php } ?>
            <?php if (isset($_GET['tanggal_lahir'])) {?>
                <th>Tanggal Lahir</th>
            <?php } ?>
            <?php if (isset($_GET['alamat'])) {?>
                <th>Alamat</th>
            <?php } ?>
            <?php if (isset($_GET['agama'])) {?>
                <th>Agama</th>
            <?php } ?>
            <?php if (isset($_GET['kebangsaan'])) {?>
                <th>Kebangsaan</th>
            <?php } ?>
            <?php if (isset($_GET['kecamatan'])) {?>
                <th>Kecamatan</th>
            <?php } ?>
            <?php if (isset($_GET['kabupaten'])) {?>
                <th>Kabupaten</th>
            <?php } ?>
            <?php if (isset($_GET['telepon'])) {?>
                <th>Telepon</th>
            <?php } ?>
            <?php if (isset($_GET['pendidikan'])) {?>
                <th>Pendidikan</th>
            <?php } ?>
            <?php if (isset($_GET['npwp'])) {?>
                <th>NPWP</th>
            <?php } ?>
            <?php if (isset($_GET['tmt_cpns'])) {?>
                <th>TMT CPNS</th>
            <?php } ?>
            <?php if (isset($_GET['tmk'])) {?>
                <th>Tanggal Mulai Kerja</th>
            <?php } ?>
            <?php if (isset($_GET['pangkat'])) {?>
                <th>Pangkat</th>
            <?php } ?>
            <?php if (isset($_GET['jenis'])) {?>
                <th>Jenis Pegawai</th>
            <?php } ?>
            <?php if (isset($_GET['status'])) {?>
                <th>Status Pegawai</th>
            <?php } ?>
            <?php if (isset($_GET['jabatan'])) {?>
                <th>Jabatan</th>
            <?php } ?>
            <?php if (isset($_GET['gaji'])) {?>
                <th>Gaji Pokok</th>
            <?php } ?>
            <?php if (isset($_GET['str'])) {?>
                <th>STR</th>
            <?php } ?>
            <?php if (isset($_GET['sip'])) {?>
                <th>SIP</th>
            <?php } ?>
            <?php if (isset($_GET['jml_keluarga'])) {?>
                <th>Jumlah Keluarga</th>
            <?php } ?>
            <?php if (isset($_GET['unit'])) {?>
                <th>Unit</th>
            <?php } ?>
            <?php if (isset($_GET['bpjs'])) {?>
                <th>BPJS</th>
            <?php } ?>
		</tr>
        <?php 
        // menampilkan data pegawai
		if ($_GET['jenis_pegawai'] == 'semua') {
            $data = mysqli_query($koneksi,"select * from pegawai");
        }
        else {
            $data = mysqli_query($koneksi,"select * from pegawai where jenis_pegawai = '$_GET[jenis_pegawai]'");
        }
		$no = 1;
		while($d = mysqli_fetch_array($data)){
		?>
		<tr>
			<td><?php echo $no++; ?></td>
            <?php if (isset($_GET['nip'])) {?>
                <td>
                    <?=$d['nip']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['nama'])) {?>
                <td>
                    <?=$d['nama']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['nik'])) {?>
                <td>
                    <?='\''.$d['nik']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['kelamin'])) {?>
                <td>
                    <?php
                        if ($d['gender'] == 'l') {
                            echo "Laki-laki";
                        }
                        else {
                            echo "Perempuan";
                        }
                    ?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['tempat_lahir'])) {?>
                <td>
                    <?=$d['tempat_lahir']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['tanggal_lahir'])) {?>
                <td>
                    <?=$d['tgl_lahir']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['alamat'])) {?>
                <td>
                    <?=$d['alamat']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['agama'])) {?>
                <td>
                    <?=$d['agama']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['kebangsaan'])) {?>
                <td>
                    <?=$d['kebangsaan']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['kecamatan'])) {?>
                <td>
                    <?=$d['kecamatan']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['kabupaten'])) {?>
                <td>
                    <?=$d['kabupaten']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['telepon'])) {?>
                <td>
                    <?='\''.$d['wa']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['pendidikan'])) {?>
                <td>
                    <?php 
                        $klf = [];
                        $id_klf = [];
                        $sql = "select * from tb_pegawai_kualifikasi where pegawai_id = '$d[id]'";
                        $sql1 = "select * from pendidikan";
                        $data_kualifikasi = mysqli_query($koneksi, $sql);  
                        $data_pendidikan = mysqli_query($koneksi, $sql1);
                        while ($a = mysqli_fetch_array($data_kualifikasi)) {
                            array_push($id_klf, $a['kualifikasi_id']);
                        }

                        while ($a = mysqli_fetch_array($data_pendidikan)) {
                            if (in_array($a['id'], $id_klf)) {
                                echo $a['kualifikasi'].', ';
                            }
                        }
                    ?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['npwp'])) {?>
                <td>
                    <?=$d['npwp']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['tmt_cpns'])) {?>
                <td>
                    <?=$d['tmt_cpns']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['tmk'])) {?>
                <td>
                    <?=$d['tmt']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['pangkat'])) {?>
                <td>
                    <?=$d['pangkat']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['jenis'])) {?>
                <td>
                    <?=$d['jenis_pegawai']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['status'])) {?>
                <td>
                    <?=$d['status_pegawai']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['jabatan'])) {?>
                <td>
                    <?=$d['jabatan']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['gaji'])) {?>
                <td>
                    <?=$d['gaji_pokok']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['str'])) {?>
                <td>
                    <?='\''.$d['str']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['sip'])) {?>
                <td>
                    <?=$d['sip']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['jml_keluarga'])) {?>
                <td>
                    <?=$d['jumlah_keluarga']?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['unit'])) {?>
                <td>
                    <?php
                        $unit = mysqli_query($koneksi, "SELECT * FROM unit WHERE id='$d[unit]'");
                        while ($row_unit = mysqli_fetch_array($unit)) {
                            echo $row_unit['unit'];
                        }
                    ?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['bpjs'])) {?>
                <td>
                    '<?=$d['bpjs']?>
                </td>
            <?php } ?>
		</tr>
		<?php 
		}
		?>
	</table>
</body>
</html>