<?php 
    include('../config/koneksi.php');
            
    // menampilkan data pegawai
    $data = mysqli_query($koneksi,"SELECT * FROM profil");

    $home = mysqli_fetch_array($data);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Export Data Karyawan Berhenti</title>
</head>
<body>
	<style type="text/css">
	body{
		font-family: sans-serif;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table#data th,
	table#data td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;

	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>

	<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Export-Data-Pegawai-".date('Y-m-d').".xls");
	?>

	<table border="0">
    <tr>
        <td></td>
        <td></td>
        <td>
            <h3>Export Data Karyawan Berhenti <?=$home['nama']?></h3>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <h3>Tanggal <?=date('Y-m-d')?></h3>
        </td>

    </tr>
    <tr></tr>
    <tr></tr>
    </table>
	

	<table border="1" id="data">
		<tr>
			<th>No</th>
            <?php if (isset($_GET['nip'])) {?>
                <th>NIP</th>
            <?php } ?>
            <?php if (isset($_GET['nama'])) {?>
                <th>Nama</th>
            <?php } ?>
            <?php if (isset($_GET['jabatan'])) {?>
                <th>Jabatan</th>
            <?php } ?>
            <?php if (isset($_GET['pendidikan'])) {?>
                <th>Pendidikan</th>
            <?php } ?>
            <?php if (isset($_GET['tmk'])) {?>
                <th>Tanggal Mulai Kerja</th>
            <?php } ?>
            <?php if (isset($_GET['tgl_keluar'])) {?>
                <th>Tanggal Keluar</th>
            <?php } ?>
            <?php if (isset($_GET['alasan'])) {?>
                <th>Alasan</th>
            <?php } ?>
		</tr>
        <?php 
        // menampilkan data pegawai
		$data = mysqli_query($koneksi,"select * from mutasi");
		$no = 1;
		while($d = mysqli_fetch_array($data)){
		?>
		<tr>
			<td><?php echo $no++; ?></td>
            <?php if (isset($_GET['nip'])) {?>
                <td><?=$d['nip'] ?></td>
            <?php } ?>
            <?php if (isset($_GET['nama'])) {?>
                <td><?=$d['nama'] ?></td>
            <?php } ?>
            <?php if (isset($_GET['jabatan'])) {?>
                <td><?=$d['jabatan'] ?></td>
            <?php } ?>
            <?php if (isset($_GET['pendidikan'])) {?>
                <td>
                    <?php 
                        $klf = [];
                        $id_klf = [];
                        $sql = "select * from tb_mutasi_pendidikan where mutasi_nip = '$d[nip]'";
                        $sql1 = "select * from pendidikan";
                        $data_kualifikasi = mysqli_query($koneksi, $sql);  
                        $data_pendidikan = mysqli_query($koneksi, $sql1);
                        while ($a = mysqli_fetch_array($data_kualifikasi)) {
                            array_push($id_klf, $a['kualifikasi_id']);
                        }

                        while ($a = mysqli_fetch_array($data_pendidikan)) {
                            if (in_array($a['id'], $id_klf)) {
                                echo $a['kualifikasi'].', ';
                            }
                        }
                    ?>
                </td>
            <?php } ?>
            <?php if (isset($_GET['tmk'])) {?>
                <td><?=$d['tmt'] ?></td>
            <?php } ?>
            <?php if (isset($_GET['tgl_keluar'])) {?>
                <td><?=$d['tgl_keluar'] ?></td>
            <?php } ?>
            <?php if (isset($_GET['alasan'])) {?>
                <td><?=$d['alasan'] ?></td>
            <?php } ?>
		</tr>
		<?php 
		}
		?>
	</table>
</body>
</html>