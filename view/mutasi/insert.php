<title><?php echo $row_data['nama']; ?> | Insert Karyawan Berhenti</title>
<?php
if ($_SESSION['level_simpeg'] == "admin" || $_SESSION['level_simpeg'] == "admin master") {
?>

    <!-- INI UNTUK JUDUL -->
    <section class="content-header">
        <h1>
            Insert Data Karyawan Berhenti
        </h1>
        <ol class="breadcrumb">
            <li><a href="index.php?controller=sistem&method=home"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="?controller=mutasi&method=select">Data Karyawan Berhenti</a></li>
            <li class="active">Insert</li>
        </ol>
    </section>
    <section class="content">
        <a href="index.php?controller=mutasi&method=select" class="btn btn-md btn-info" data-toggle="tooltip" data-placement="top" title="Kembali"><i class="fa fa-arrow-left fa-fw"></i>Kembali</a>
        <br>
        <br>
        <!-- INI UNTUK ISI -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <!-- INI BAGIAN ISI UNTUK JUDUL TABEL -->
                    <div class="panel-heading bg-aqua">
                        <i class="fa fa-users fa-fw"></i> Insert data
                    </div>

                    <!-- INI BAGIAN ISI UTAMA -->
                    <div class="panel-body table-responsive">
                        <!-- INI BAGIAN TABEL -->
                        <form name="kategori" action="index.php?controller=mutasi&method=insert_mutasi" method="POST" onsubmit="return validasi(); " enctype="multipart/form-data">
                            <table width="100%">
                                <hr>
                                <!--baris 1-->
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            Nip
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group" style="display: inline-block;">
                                            <input name="nip" id="nip" class="form-control" placeholder="Nip" autocomplete="off" onkeypress="return setNip(event);" required=""></input>
                                        </div>
                                        <a class="btn btn-md btn-info" id="getData" onclick="getData()">
                                            <i class="fa fa-search"></i>
                                        </a>
                                    </td>
                                    <!-- <td colspan="2"></td> -->
                                    
                                </tr>
                                    
                    </table>
                    <table id="response">
                    
                    </table>
                    <br>
                    <center>
                        <a href="?controller=pegawai&method=select" class="btn btn-md btn-success"><i class="fa fa-close fa-fw"></i> Batal</a>
                        <button class="btn btn-md btn-primary"><i class="fa fa-download fa-fw"></i> Simpan</button>
                    </center>
                    </form>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>

    <script type="text/javascript">
        function CanInput() {
            var e = document.getElementById("jabatan");
            var jabatan = e.value;
            if (jabatan == "Dokter" || jabatan == "Perawat" || jabatan == "Apoteker") {
                // document.getElementById("str").readOnly = false;
                // document.getElementById("sip").readOnly = false;
                document.getElementById("kualifikasi").disabled = false;
            } else {
                // document.getElementById("str").readOnly = true;
                // document.getElementById("sip").readOnly = true;
                document.getElementById("kualifikasi").disabled = true;
                // document.getElementById("sip").value = null;
                // document.getElementById("str").value = null;
                document.getElementById("kualifikasi").value = null;
            }

        }
        // 01.855.081.4-412.000
        // 19580818.198404.1.001
        function angka(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        
        function setNip(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
                return false;
            }
            return true;
        }

        function setNpwp(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 45 || charCode > 57 || charCode == 47)) {
                return false;
            }
            return true;
        }

        function getData() {
            let nip = document.getElementById("nip");
            // alert(nip.value);

            var xhttp;

            if (nip == "") {
                // document.getElementById("txtHint").innerHTML = "";
                return;
            }
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    // alert(this.responseText);
                    document.getElementById("response").innerHTML = this.responseText;
                }
            };
            
            xhttp.open("GET", "getDataPegawai.php?q="+nip.value, true);
            xhttp.send();
        }
        
    </script>

    <script language='javascript'>
        function validasi() {
            if (document.kategori.hari.selectedIndex == 0) {
                alert("Pilih Tanggal Lahir");
                document.kategori.hari.focus();
                return false;
            }
            if (document.kategori.bulan.selectedIndex == 0) {
                alert("Pilih Bulan Kelahiran");
                document.kategori.bulan.focus();
                return false;
            }
            if (document.kategori.tahun.selectedIndex == 0) {
                alert("Pilih Tahun Kelahiran");
                document.kategori.tahun.focus();
                return false;
            }
            if (document.kategori.pangkat.selectedIndex == 0) {
                alert("Pilih Pangkat / Golongan");
                document.kategori.pangkat.focus();
                return false;
            }
            if (document.kategori.jenis_pegawai.selectedIndex == 0) {
                alert("Pilih Jenis Kepegawaian");
                document.kategori.jenis_pegawai.focus();
                return false;
            }
            if (document.kategori.status_pegawai.selectedIndex == 0) {
                alert("Pilih Status Kepegawaian");
                document.kategori.status_pegawai.focus();
                return false;
            }
            if (document.kategori.jabatan.selectedIndex == 0) {
                alert("Pilih Jabatan Strukural");
                document.kategori.jabatan.focus();
                return false;
            }
            if (document.kategori.agama_pegawai.selectedIndex == 0) {
                alert("Pilih Agama");
                document.kategori.agama_pegawai.focus();
                return false;
            }
            if (document.kategori.sk_terakhir.selectedIndex == 0) {
                alert("Pilih Sk");
                document.kategori.sk_terakhir.focus();
                return false;
            }
            return true
        }
    </script>

    <script type="text/javascript">
        /* Tunjangan */
        //gaji pokok
        var gaji_pokok = document.getElementById('gaji_pokok');
        gaji_pokok.addEventListener('keyup', function(e) {
            gaji_pokok.value = formatRupiah(this.value);
        });

        /* Fungsi */
        function formatRupiah(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }
    </script>

<?php
} else {
    echo "<script>window.history.back(); </script>";
}
?>