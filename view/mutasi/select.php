<title><?php echo $row_data['nama']; ?> | Data Gaji Pegawai </title>
<?php
date_default_timezone_set('Asia/Jakarta');
if ($_SESSION['level_simpeg'] == "admin" || $_SESSION['level_simpeg'] == "admin master") {
?>


  <!-- INI UNTUK JUDUL -->
  <section class="content-header">
    <h1>
      Data Karyawan Berhenti
    </h1>
    <ol class="breadcrumb">
      <li><a href="index.php?controller=sistem&method=home"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">Data Karyawan Berhenti</li>
    </ol>
  </section>
  <section class="content">
  <?php
    if ($_SESSION['level_simpeg'] == "admin" || $_SESSION['level_simpeg'] == "admin master") { ?>
        <a href="index.php?controller=mutasi&method=insert" class="btn btn-md btn-info" data-toggle="tooltip" data-placement="top" title="Tambah Data"><i class="fa fa-plus fa-fw"></i>Tambah Data</a>
        <!-- <a href="laporan/cetak_mutasi.php" target="_blank" class="btn btn-md btn-primary pull-right" data-toggle="tooltip" data-placement="top" title="Cetak"><i class="fa fa-print"></i></a> -->
        <button type="button" class="btn btn-primary btn-md pull-right" data-toggle="modal" data-target="#cetakPegawai" title="Cetak" data-placement="top">
            <i class="fa fa-print"></i>
        </button>
        <br>
        <br>
    <?php } ?>
    <!-- INI UNTUK ISI -->
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <!-- INI BAGIAN ISI UNTUK JUDUL TABEL -->
          <div class="panel-heading bg-aqua">
            <i class="fa fa-line-chart fa-fw"></i> Data Karyawan Berhenti
          </div>

          <!-- INI BAGIAN ISI UTAMA -->
          <div class="panel-body table-responsive">
            <!-- INI BAGIAN TABEL -->

            <table width="100%" id="tabel" class="table table-striped table-bordered table-hover">
              <thead>
                <tr class="odd bg-gray">
                  <th width="1%" valign="top">No</th>
                  <th>
                    <center>Nip</center>
                  </th>
                  <th>
                    <center>Nama Pegawai</center>
                  </th>
                  <th>
                    <center>Jabatan</center>
                  </th>
                  <th>
                    <center>Pendidikan</center>
                  </th>
                  <th>
                    <center>Tanggal Mulai Kerja
                    </center>
                  </th>
                  <th>
                    <center>Tanggal Keluar</center>
                  </th>
                  <th>
                    <center>Alasan</center>
                  </th>
                  <th>
                    <center>Berkas</center>
                  </th>
                  <th>
                    <center>Aksi</center>
                  </th>
                </tr>
              </thead>
              <!-- INI UNTUK MENERIMA DATA DARI CONTROLLER -->
              <tbody>
                <?php
                // SET NOMOR URUT DATA
                $nomor          =   1;

                // CEK DATA YANG DITERIMA
                if (isset($data_pegawai)) {
                  while ($row_pegawai  = mysqli_fetch_array($data_pegawai)) {
                ?>

                    <tr class="odd gradeX">
                      <td><?php echo $nomor; ?></td>
                      <td><?php echo $row_pegawai['nip']; ?></td>
                      <td><?php echo $row_pegawai['nama']; ?></td>
                      <td><?php echo $row_pegawai['jabatan']; ?></td>
                      <td>
                        <?php
                          include("config/koneksi.php");
                          $query_kualifikasi = "select * from pendidikan";
                          $kualifikasi = mysqli_query($koneksi, $query_kualifikasi);
                          $tb = "select * from tb_mutasi_pendidikan where mutasi_nip='$row_pegawai[nip]'";
                          $tb_data = mysqli_query($koneksi, $tb);
                          $array = [];
                          $i = 0;
                          while ($row_tb = mysqli_fetch_array($tb_data)) {
                              array_push($array, $row_tb['kualifikasi_id']);
                          }
                          
                          while ($row_kualifikasi = mysqli_fetch_array($kualifikasi)) {
                              // MENAMPILKAN OPSI Kategori
                              if($row_kualifikasi['id'] == $array[$i]) {
                                  $i++;
                                  if ($i == count($array)) {
                                    echo $row_kualifikasi['kualifikasi'];                                  
                                  }
                                  else {
                                    echo $row_kualifikasi['kualifikasi'].', ';
                                  }
                              }
                              if ($i == count($array)) {
                                  break;
                              }
                              // echo "<li>".$row_kualifikasi['kualifikasi']."</li>";
                              // echo "<option value=".$row_kualifikasi['id'].">".$row_kualifikasi['kualifikasi']."</option>";
                              
                          }
                        ?>
                      </td>
                      <td align="center"><?php echo $row_pegawai['tmt']; ?>
                      </td>
                      <td>
                        <?php echo $row_pegawai['tgl_keluar']; ?>
                      </td>
                      <td>
                        <?php echo $row_pegawai['alasan']; ?>
                      </td>
                      <td>
                        <a href="berkas/karyawanberhenti/<?= $row_pegawai['file'] ?>" class='btn btn-info btn-sm'><i class="fa fa-file"></i></a>
                      </td>
                      <!-- <?php
                      $q_mutasi = mysqli_query($koneksi, "SELECT * FROM mutasi WHERE nip='$row_pegawai[nip]'");
                      $mutasi = mysqli_fetch_array($q_mutasi);
                      ?> -->
                      <!-- pangkat -->
                      
                      <td>
                        <center>
                          <!-- <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-pangkat<?php echo $row_pegawai['nip']; ?>"> -->
                          <a class="btn btn-danger btn-xs" href="index.php?controller=mutasi&method=delete&nip=<?=$row_pegawai['nip']?>">
                            <li class="fa fa-trash"></li> Hapus
                          </a>
                          <!-- </button> -->
                        </center>    
                      </td>
                    </tr>
                <?php
                    // INCREMENT NOMOR URUT
                    $nomor++;
                  }
                }
                ?>
              </tbody>

            </table>

          </div>
        </div>
      </div>
    </div>
    </div>
    <center>
    <!--Modal Untuk edit Data -->
    <div class="modal modal-primary fade" id="cetakPegawai">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <center>
            <h4 class="modal-title">Cetak Karyawan Berhenti</h4>
            </center>
        </div>
        <form role="form" method="GET" action="laporan/cetak_mutasi.php">
            <table width="100%" id="tabel" class="table table-striped table-bordered table-hover">
                <tr>
                    <td><input type="checkbox" name="nip" id="" class="checkbox-success" id="nipp" checked> NIP</td>
                    <td><input type="checkbox" name="nama" id="" class="checkbox-success" checked> NAMA</td>
                </tr>
                <tr>
                    <!-- <td><input type="checkbox" name="npwp" id="" class="checkbox-success" checked> NPWP</td> -->
                    <td><input type="checkbox" name="tmk" id="" class="checkbox-success" checked> TANGGAL MULAI KERJA</td>
                    <td><input type="checkbox" name="jabatan" id="" class="checkbox-success" checked> JABATAN</td>
                    <!-- <td><input type="checkbox" name="pangkat" id="" class="checkbox-success" checked> PANGKAT</td> -->
                    <!-- <td><input type="checkbox" name="jenis" id="" class="checkbox-success" checked> JENIS PEGAWAI</td> -->
                </tr>
                <tr>
                    <!-- <td><input type="checkbox" name="sip" id="" class="checkbox-success" checked> SIP</td> -->
                    <td><input type="checkbox" name="tgl_keluar" id="" class="checkbox-success" checked> TANGGAL KELUAR</td>
                    <td><input type="checkbox" name="pendidikan" id="" class="checkbox-success" checked> PENDIDIKAN</td>
                    <!-- <td><input type="checkbox" name="" id="" class="checkbox-success" checked></td> -->
                </tr>
                <tr>
                    <td><input type="checkbox" name="alasan" id="" class="checkbox-success" checked> ALASAN</td>
                </tr>
            </table>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <input id="button" type="submit" name="submit" class="btn btn-outline btn-xl" value="Unduh" data-toggle="tooltip" data-placement="top" title="Unduh">
            </div>
        </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


</center>

    <script type='text/javascript'>
      function caripegawai() {
        var lol = "<?php echo 1
                    ?>";
        console.log(lol);
      }

      function angka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
        }
        return true;
      }
    </script>
  </section>




  <script language='javascript'>
    function validasi() {
      if (document.kategori.bulan.selectedIndex == 0) {
        alert("Pilih Bulan Kelahiran");
        document.kategori.bulan.focus();
        return false;
      }
      if (document.kategori.tahun.selectedIndex == 0) {
        alert("Pilih Tahun Kelahiran");
        document.kategori.tahun.focus();
        return false;
      }
      if (document.kategori.pangkat.selectedIndex == 0) {
        alert("Pilih Pangkat / Golongan");
        document.kategori.pangkat.focus();
        return false;
      }
      return true
    }
  </script>


<?php
} else {
  echo "<script>window.history.back(); </script>";
}
?>