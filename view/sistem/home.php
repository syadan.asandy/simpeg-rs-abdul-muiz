 <script>
   // Initialize and add the map
   function initMap() {
     // The location of Uluru
     var uluru = {
       lat: -2.1634076,
       lng: 106.1695294
     };
     // The map, centered at Uluru
     var map = new google.maps.Map(
       document.getElementById('map'), {
         zoom: 17,
         center: uluru
       });
     // The marker, positioned at Uluru
     var marker = new google.maps.Marker({
       position: uluru,
       map: map,
       title: 'Nama Maps'
     });
   }
 </script>
 <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyY0ZSEe2hO5U1aOaJ2spgAw60s2gYsds" async defer></script>

 <title><?php echo $row_data['nama']; ?> | Dashboard</title>


 <?php
  date_default_timezone_set('Asia/Jakarta');
  ?>
 <script src="bootstrap/chart/js/highcharts.js"></script>

 <script src="bootstrap/chart/js/jquery-1.10.1.min.js"></script>

 <script>
   var chart;
   $(document).ready(function() {
     chart = new Highcharts.Chart({

       chart: {
         renderTo: 'mygraph',
         plotBackgroundColor: null,
         plotBorderWidth: null,
         plotShadow: false
       },
       title: {
         text: 'Grafik Data Pegawai '
       },
       tooltip: {
         formatter: function() {
           return '<b>' +
             this.point.name + '</b>: ' + Highcharts.numberFormat(this.percentage, 2) + ' % ';
         }
       },


       plotOptions: {
         pie: {
           allowPointSelect: true,
           cursor: 'pointer',
           dataLabels: {
             enabled: true,
             color: '#000000',
             connectorColor: 'green',
             formatter: function() {
               return '<b>' + this.point.name + '</b>: ' + Highcharts.numberFormat(this.percentage, 2) + ' % ';
             }
           }
         }
       },

       series: [{
         type: 'pie',
         name: 'pegawai share',
         data: [
           <?php
            include "config/koneksi.php";
            $query = mysqli_query($koneksi, "SELECT * from pegawai GROUP BY jenis_pegawai");
            while ($row = mysqli_fetch_array($query)) {
              $jenis = $row['jenis_pegawai'];

              $data = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from pegawai where jenis_pegawai='$jenis'"));
              $jumlah = $data;
            ?>[
               '<?=$jenis ?>', <?php echo $jumlah; ?>
             ],
           <?php
            }
            ?>

         ],
       }]
     });
   });
 </script>

 <!-- INI UNTUK JUDUL -->
 <section class="content-header">
   <h1>Dashboard
     <small>Control panel</small>
   </h1>
   <ol class="breadcrumb">
     <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
     <li class="active"></i>Dashboard</li>
   </ol>
 </section>
 <section class="content-header">
   <section class="content-header bg-teal" style="border-radius: 5px;">

     <marquee><b>Selamat Datang Di <?php
                                                                                                                  echo $row_data['nama'];
                                                                                                                  ?> <?php
                                                                                                                      echo $row_data['instansi'];
                                                                                                                      ?>
       </b></marquee>
   </section>
 </section>
 <section class="content">

   <table width="100%">
     <tr>
       <td align="center" width="15%">
         <img src="logo/bm.png" style="width: 100px;">
       </td>
       <td width="85%">
         <h2><b style="color: orange;"><?php echo $row_data['nama']; ?><br>
             <?php echo $row_data['instansi']; ?></b></h2>
         <p style="color: black;text-shadow: 0 0 5px white;"><b><?php echo $row_data['alamat']; ?></b></p>
       </td>
     </tr>
   </table>
   <br><br>
   <!-- INI UNTUK ISI -->
   <!-- INI UNTUK ISI -->
   <div class="row">

     <div class="col-lg-6 col-xs-12 col-md-6">
       <!-- small box -->
       <div class="small-box bg-aqua">
         <div class="panel panel-primary">
           <div class="panel-heading">
             <center>Grafik Data Pegawai <?php echo $row_data['nama']; ?></center>
           </div>
           <div class="panel-body">
             <div id="mygraph"></div>
           </div>
         </div>
       </div>
     </div>

     <?php
      $jml = mysqli_num_rows($data_pegawai);
      
      include "config/koneksi.php";
      $getJenis = "SELECT * FROM jabatan where jenis = 'jenis'";
      $query2 = mysqli_query($koneksi, $getJenis);
      $i = 0;
      // $query = mysqli_query($koneksi, "SELECT count(*)  FROM `pegawai` WHERE jenis_pegawai='Pegawai Negeri Sipil'");
      // $query1 = mysqli_query($koneksi, "SELECT count(*)  FROM `pegawai` WHERE jenis_pegawai='honorer'");

      $color = ['#AA4643', '#4572A7', '#89A54E', '#80699B', '#3D96AE',
      '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'];
      // $jmlH = mysqli_fetch_array($query1);

      ?>
     <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <p>Jumlah Pegawai</p>
                <p style="font-size: 25px;" align="center"><b><?php echo $jml; ?></b></p>
              </div>
              <div class="icon">
                <i class="fa fa-street-view fa-2"></i>
              </div>
              <a href="index.php?controller=pegawai&method=select&jenis=semua" class="small-box-footer">Lihat Data Pegawai <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
        
        <div class="row">
            <?php while ($a = mysqli_fetch_array($query2)) {
              $query = mysqli_query($koneksi, "SELECT count(*)  FROM `pegawai` WHERE jenis_pegawai='$a[nama]'");
              $n = mysqli_fetch_array($query);
          ?>
              <div class="col-lg-6 col-xs-6">
              <!-- small box -->
              <div class="small-box" style="background-color: <?=$color[$i]?>;">
                <div class="inner">
                  <p><?=$a['nama'] ?></p>
                  <p style="font-size: 25px;" align="center"><b><?php echo $n[0]; ?></b></p>
                </div>
                <div class="icon">
                  <i class="fa fa-user"></i>
                </div>
                <a href="index.php?controller=pegawai&method=select&jenis=<?=$a['nama']?>" class="small-box-footer">Lihat Data Pegawai <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
          <?php $i++; } ?>
          </div>
     </div>

     <!-- Hanya Admin Master dan Admin dapat melihat pengguna -->
     
   </div>
   <section>
     <div id="map" style="width:100%;height:400px;"></div>
   </section>

 </section>
 <section>
   </div>
   </div>
 </section>




 <script src="bootstrap/chart/js/highcharts.js"></script>
 <script src="bootstrap/chart/js/jquery-1.10.1.min.js"></script>