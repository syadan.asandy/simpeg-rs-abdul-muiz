<title><?php echo $row_data['nama']; ?> | Pegawai</title>

<!-- INI UNTUK JUDUL -->
<section class="content-header">
    <h1>
        Data Pegawai
    </h1>
    <ol class="breadcrumb">
        <li><a href="index.php?controller=sistem&method=home"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Data Pegawai</li>
    </ol>
</section>
<section class="content">
    <?php
    if ($_SESSION['level_simpeg'] == "admin" || $_SESSION['level_simpeg'] == "admin master") { ?>
        <a href="index.php?controller=pegawai&method=insert&jenis=<?=$jenis?>" class="btn btn-md btn-info" data-toggle="tooltip" data-placement="top" title="Tambah Data"><i class="fa fa-plus fa-fw"></i>Tambah Data</a>
        <!-- <a href="laporan/cetak_pegawai.php" target="_blank" class="btn btn-md btn-primary pull-right" data-toggle="tooltip" data-placement="top" title="Cetak"><i class="fa fa-print"></i></a> -->
        <button type="button" class="btn btn-primary btn-md pull-right" data-toggle="modal" data-target="#cetakPegawai" title="Cetak" data-placement="top">
            <i class="fa fa-print"></i>
        </button>
        <br>
        <br>
    <?php } ?>
    <!-- INI UNTUK ISI -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- INI BAGIAN ISI UNTUK JUDUL TABEL -->
                <div class="panel-heading bg-aqua">
                    <i class="fa fa-users fa-fw"></i> Data Pegawai
                </div>

                <!-- INI BAGIAN ISI UTAMA -->
                <div class="panel-body table-responsive">
                    <!-- INI BAGIAN TABEL -->
                    <table width="100%" id="tabel" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr class="odd bg-gray">
                                <th width="5%">No</th>
                                <th>
                                    <center>Nip</center>
                                </th>
                                <th>
                                    <center>Nama Pegawai</center>
                                </th>
                                <th>
                                    <center>Jabatan</center>
                                </th>
                                <?php
                                if ($_SESSION['level_simpeg'] == "user") { ?>
                                    <th>
                                        <center>Pangkat / Golongan</center>
                                    </th>
                                    <th>
                                        <center>TMT Pangkat / Golongan</center>
                                    </th>
                                <?php } ?>
                                <th>
                                    <center>Gender</center>
                                </th>
                                <th>
                                    <center>Pendidikan</center>
                                </th>
                                <th>
                                    <center>Jenis</center>
                                </th>
                                <?php
                                if ($_SESSION['level_simpeg'] == "admin" || $_SESSION['level_simpeg'] == "admin master") { ?>
                                    <th>
                                        <center>Aksi</center>
                                    </th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <!-- INI UNTUK MENERIMA DATA DARI CONTROLLER -->
                        <tbody>
                            <?php
                            // SET NOMOR URUT DATA
                            $nomor          =   1;

                            // CEK DATA YANG DITERIMA
                            if (isset($data_pegawai)) {
                                while ($row_pegawai  = mysqli_fetch_array($data_pegawai)) {
                            ?>

                                    <tr class="odd gradeX">
                                        <td align="center"><?php echo $nomor; ?></td>
                                        <td><?php echo $row_pegawai['nip']; ?></td>
                                        <td><?php echo $row_pegawai['nama']; ?></td>
                                        <td><?php echo $row_pegawai['jabatan']; ?></td>

                                        <?php
                                        if ($_SESSION['level_simpeg'] == "user") { ?>
                                            <td><?php echo $row_pegawai['pangkat']; ?></td>
                                            <td align="center"><?php echo TanggalIndo($row_pegawai['tmt_golongan']); ?></td>
                                        <?php } ?>

                                        <td>
                                            <?php
                                            if ($row_pegawai['gender'] == "l") {
                                                echo "Laki - Laki";
                                            } else {
                                                echo "Perempuan";
                                            } ?>
                                        </td>

                                        <?php
                                        if ($_SESSION['level_simpeg'] == "admin" || $_SESSION['level_simpeg'] == "admin master") { ?>
                                            <td>
                                                <?php
                                                    $data_kualifikasi = $this->pegawai->dataDetailKualifikasi($row_pegawai['nip']);
                                                    while ($a = mysqli_fetch_row($data_kualifikasi)) {
                                                        echo $a[0].', ';
                                                    }
                                                ?>
                                            </td>
                                            <!-- <td><?php echo $row_pegawai['agama']; ?></td> -->
                                        <?php } ?>
                                        <td><?php echo $row_pegawai['jenis_pegawai']; ?></td>
                                        <?php
                                        if ($_SESSION['level_simpeg'] == "admin" || $_SESSION['level_simpeg'] == "admin master") { ?>
                                            <td>
                                                <center>
                                                    <a href="index.php?controller=pegawai&method=detail&nip=<?php echo $row_pegawai['nip']; ?>&jenis=<?=$jenis?>" class="btn btn-primary btn-xs" role="button" data-toggle="tooltip" data-placement="top" title="Detail"> <i class="fa fa-info fa-fw"></i> </a>

                                                    <a href="index.php?controller=pegawai&method=delete&nip=<?php echo $row_pegawai['nip']; ?>&jenis=<?=$jenis?>" class="btn btn-danger btn-xs" role="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="return confirm('Yakin ingin menghapus data NIP : <?php echo $row_pegawai['nip']; ?> ?')"> <i class="fa fa-trash fa-fw"></i> </a>
                                                </center>
                                            </td>
                                        <?php } ?>
                                    </tr>

                            <?php
                                    // INCREMENT NOMOR URUT
                                    $nomor++;
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- /.modal -->
<center>
    <!--Modal Untuk edit Data -->
    <div class="modal modal-primary fade" id="cetakPegawai">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <center>
            <h4 class="modal-title">Cetak Pegawai <?=ucfirst($jenis)?></h4>
            </center>
        </div>
        <form role="form" method="GET" action="laporan/cetak_pegawai.php">
        <input type="hidden" name="jenis_pegawai" value="<?=$jenis?>">
            <table width="100%" id="tabel" class="table table-striped table-bordered table-hover">
                <tr>
                    <td><input type="checkbox" name="nip" id="" class="checkbox-success" id="nipp" checked> NIP</td>
                    <td><input type="checkbox" name="nama" id="" class="checkbox-success" checked> NAMA</td>
                    <td><input type="checkbox" name="nik" id="" class="checkbox-success" checked> NIK</td>
                    <td><input type="checkbox" name="kelamin" id="" class="checkbox-success" checked> KELAMIN</td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="tempat_lahir" id="" class="checkbox-success" checked> TEMPAT LAHIR</td>
                    <td><input type="checkbox" name="tanggal_lahir" id="" class="checkbox-success" checked> TANGGAL LAHIR</td>
                    <td><input type="checkbox" name="agama" id="" class="checkbox-success" checked> AGAMA</td>
                    <td><input type="checkbox" name="alamat" id="" class="checkbox-success" checked> ALAMAT</td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="kebangsaan" id="" class="checkbox-success" checked> KEBANGSAAN</td>
                    <td><input type="checkbox" name="kecamatan" id="" class="checkbox-success" checked> KECAMATAN</td>
                    <td><input type="checkbox" name="kabupaten" id="" class="checkbox-success" checked> KABUPATEN</td>
                    <td><input type="checkbox" name="npwp" id="" class="checkbox-success" checked> NPWP</td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="tmt_cpns" id="" class="checkbox-success" checked> TMT CPNS</td>
                    <td><input type="checkbox" name="tmk" id="" class="checkbox-success" checked> TANGGAL MULAI KERJA</td>
                    <td><input type="checkbox" name="pangkat" id="" class="checkbox-success" checked> PANGKAT</td>
                    <td><input type="checkbox" name="jenis" id="" class="checkbox-success" checked> JENIS PEGAWAI</td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="status" id="" class="checkbox-success" checked> STATUS PEGAWAI</td>
                    <td><input type="checkbox" name="jabatan" id="" class="checkbox-success" checked> JABATAN</td>
                    <td><input type="checkbox" name="gaji" id="" class="checkbox-success" checked> GAJI POKOK</td>
                    <td><input type="checkbox" name="str" id="" class="checkbox-success" checked> STR</td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="sip" id="" class="checkbox-success" checked> SIP</td>
                    <td><input type="checkbox" name="unit" id="" class="checkbox-success" checked> UNIT</td>
                    <td><input type="checkbox" name="jml_keluarga" id="" class="checkbox-success" checked> JUMLAH KELUARGA</td>
                    <td><input type="checkbox" name="pendidikan" id="" class="checkbox-success" checked> PENDIDIKAN</td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="bpjs" id="" class="checkbox-success" checked> BPJS</td>
                    <td><input type="checkbox" name="telepon" id="" class="checkbox-success" checked> TELEPON</td>
                    <!-- <td><input type="checkbox" name="jml_keluarga" id="" class="checkbox-success" checked> JUMLAH KELUARGA</td>
                    <td><input type="checkbox" name="pendidikan" id="" class="checkbox-success" checked> PENDIDIKAN</td>
                    <td><input type="checkbox" name="" id="" class="checkbox-success" checked></td> -->
                </tr>
            </table>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <input id="button" type="submit" name="submit" class="btn btn-outline btn-xl" value="Unduh" data-toggle="tooltip" data-placement="top" title="Unduh">
            </div>
        </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


</center>


<script type="text/javascript">
    function angka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>