-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2021 at 03:23 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

-- SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
-- SET AUTOCOMMIT = 0;
-- START TRANSACTION;
-- SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simpeg`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--


--
-- Dumping data for table `absensi`
--
INSERT INTO `pendidikan` (`id`, `kualifikasi`, `keterangan`) VALUES
-- TENAGA MEDIS
('', 'Dokter Umum', '-'),
('', 'Dokter PPDS *)', '-'),
('', 'Dokter Spes Bedah', '-'),
('', 'Dokter Spes Penyakit Dalam', '-'),
('', 'Dokter Spes Kes. Anak', '-'),
('', 'Dokter Spes Obgin', '-'),
('', 'Dokter Spes Radiologi', '-'),
('', 'Dokter Spes Onkologi Radiasi', '-'),
('', 'Dokter Spes Kedokteran Nuklir', '-'),
('', 'Dokter Spes Anesthesi', '-'),
('', 'Dokter Spes Patologi Klinik', '-'),
('', 'Dokter Spes Jiwa', '-'),
('', 'Dokter Spes Mata', '-'),
('', 'Dokter Spes THT', '-'),
('', 'Dokter Spes Kulit dan Kelamin', '-'),
('', 'Dokter Spes Kardiologi', '-'),
('', 'Dokter Spes Paru', '-'),
('', 'Dokter Spes Saraf', '-'),
('', 'Dokter Spes Bedah Saraf', '-'),
('', 'Dokter Spes Bedah Orthopedi', '-'),
('', 'Dokter Spes Urologi', '-'),
('', 'Dokter Spes Patologi Anatomi', '-'),
('', 'Dokter Spes Patologi Forensik', '-'),
('', 'Dokter Spes Rehabilitasi Medik', '-'),
('', 'Dokter Spes Bedah Plastik', '-'),
('', 'Dokter Spes Ked. Olah Raga', '-'),
('', 'Dokter Spes Mikrobiologi Klinik', '-'),
('', 'Dokter Spes Parasitologi Klinik', '-'),
('', 'Dokter Spes Gizi Medik', '-'),
('', 'Dokter Spes Farma Klinik', '-'),
('', 'Dokter Spes Lainnya', '-'),
('', 'Dokter Sub Spesialis Lainnya', '-'),
('', 'Dokter Gizi', '-'),
('', 'Dokter Gigi Spesialis', '-'),
('', 'Dokter/Dokter Gigi MHA/MARS **)', '-'),
('', 'Dokter/Dokter Gigi S2/S3 Kes Masy **)', '-'),
('', 'S3 (Dokter Konsultan) ***)', '-'),
-- TENAGA KEPERAWATAN
('', 'S3 Keperawatan', '-'),
('', 'S2 Keperawatan', '-'),
('', 'S1 Keperawatan', '-'),
('', 'D4 Keperawatan', '-'),
('', 'Perawat Vokasional', '-'),
('', 'Perawat Spesialis', '-'),
('', 'Pembantu Keperawatan', '-'),
('', 'S3 Kebidanan', '-'),
('', 'S2 Kebidanan', '-'),
('', 'S1 Kebidanan', '-'),
('', 'D3 Kebidanan', '-'),
('', 'Tenaga Keperawatan Lainnya', '-'),
-- KEFARMASIAN
('', 'S3 Farmasi / Apoteker', '-'),
('', 'S2 Farmasi / Apoteker', '-'),
('', 'Apoteker', '-'),
('', 'S1 Farmasi / Farmakologi Kimia', '-'),
('', 'AKAFARMA *)', '-'),
('', 'Analisis Farmasi', '-'),
('', 'Asisten Apoteker / SMF', '-'),
('', 'ST Lab Kimia Farmasi', '-'),
('', 'Tenaga Kefarmasian Lainnya', '-'),
-- KESEHATAN MASYARAKAT
('', 'S3 Kesehatan Masyarakat', '-'),
('', 'S3 Epidemiologi', '-'),
('', 'S3 Psikologi', '-'),
('', 'S2 Kesehatan Masyarakat', '-'),
('', 'S2 Epidemiologi', '-'),
('', 'S2 Biomedik', '-'),
('', 'S2 Psikologi', '-'),
('', 'S1 Kesehatan Masyarakat', '-'),
('', 'S1 Psikologi', '-'),
('', 'D3 Kesehatan Masyarakat', '-'),
('', 'D3 Sanitarian', '-'),
('', 'D1 Sanitarian', '-'),
('', 'Tenaga Kesehatan Masyarakat Lainnya', '-'),
-- GIZI
('', 'S3 Gizi / Dietisien', '-'),
('', 'S2 Gizi / Dietisien', '-'),
('', 'S1 Gizi / Dietisien', '-'),
('', 'D4 Gizi / Dietisien', '-'),
('', 'Akademi / D3 Gizi / Dietisien', '-'),
('', 'D1 Gizi / Dietisien', '-'),
('', 'Tenaga Gizi Lainnya', '-'),
-- KETERAPIAN FISIK
('', 'S1 Fisio Terapis', '-'),
('', 'D3 Fisio Terapis', '-'),
('', 'D3 Okupasi Terapis', '-'),
('', 'D3 Terapi Wicara', '-'),
('', 'D3 Orthopedi', '-'),
('', 'D3 Akupuntur', '-'),
('', 'Tenaga Keterapian Fisik Lainnya', '-'),
-- KETEKNISIAN MEDIS
('', 'S3 Opto Elektronika & Apl Laser', '-'),
('', 'S2 Opto Elektronika & Apl Laser', '-'),
('', 'Radiografer', '-'),
('', 'Radioterapis (Non Dokter)', '-'),
('', 'D4 Fisika Medik', '-'),
('', 'D3 Teknik Gigi', '-'),
('', 'D3 Teknik Radiologi & Radioterapi', '-'),
('', 'D3 Refraksionis Optisien', '-'),
('', 'D3 Perekam Medis', '-'),
('', 'D3 Teknik Elektromedik', '-'),
('', 'D3 Analis Kesehatan', '-'),
('', 'D3 Informasi Kesehatan', '-'),
('', 'D3 Kardiovaskular', '-'),
('', 'D3 Orthotik Prostetik', '-'),
('', 'D1 Teknik Tranfusi', '-'),
('', 'Teknik Gigi', '-'),
('', 'Tenaga IT dengan Teknologi Nano', '-'),
('', 'Teknisi Patologi Anatomi', '-'),
('', 'Teknisi Kardiovaskuler', '-'),
('', 'Teknisi Elektromedis', '-'),
('', 'Akupuntur Terapi', '-'),
('', 'Analis Kesehatan', '-'),
('', 'Tenaga Keterapian Fisik Lainnya', '-'),
-- TENAGA NON KESEHATAN
-- DOKTORAL
('', 'S3 Biologi', '-'),
('', 'S3 Kimia', '-'),
('', 'S3 Ekonomi / Akuntansi', '-'),
('', 'S3 Administrasi', '-'),
('', 'S3 Hukum', '-'),
('', 'S3 Teknik', '-'),
('', 'S3 Kes. Sosial', '-'),
('', 'S3 Fisika', '-'),
('', 'S3 Komputer', '-'),
('', 'S3 Statistik', '-'),
('', 'Doktoral Lainnya (S3)', '-'),
-- PASCA SARJANA
('', 'S2 Biologi', '-'),
('', 'S2 Kimia', '-'),
('', 'S2 Ekonomi / Akuntansi', '-'),
('', 'S2 Administrasi', '-'),
('', 'S2 Hukum', '-'),
('', 'S2 Teknik', '-'),
('', 'S2 Kes. Sosial', '-'),
('', 'S2 Fisika', '-'),
('', 'S2 Komputer', '-'),
('', 'S2 Statistik', '-'),
('', 'S2 Administrasi Kes. Masy', '-'),
('', 'Pasca Sarjana Lainnya (S2)', '-'),
-- SARJANA
('', 'Sarjana Biologi', '-'),
('', 'Sarjana Kimia', '-'),
('', 'Sarjana Ekonomi / Akuntansi', '-'),
('', 'Sarjana Administrasi', '-'),
('', 'Sarjana Hukum', '-'),
('', 'Sarjana Teknik', '-'),
('', 'Sarjana Kes. Sosial', '-'),
('', 'Sarjana Fisika', '-'),
('', 'Sarjana Komputer', '-'),
('', 'Sarjana Statistik', '-'),
('', 'Sarjana Lainnya (S1)', '-'),
-- SARJANA MUDA
('', 'Sarjana Muda Biologi', '-'),
('', 'Sarjana Muda Kimia', '-'),
('', 'Sarjana Muda Ekonomi / Akuntansi', '-'),
('', 'Sarjana Muda Administrasi', '-'),
('', 'Sarjana Muda Hukum', '-'),
('', 'Sarjana Muda Teknik', '-'),
('', 'Sarjana Muda Kes. Sosial', '-'),
('', 'Sarjana Muda Sekretaris', '-'),
('', 'Sarjana Muda Komputer', '-'),
('', 'Sarjana Muda Statistik', '-'),
('', 'Sarjana Muda / D3 Lainnya', '-'),
-- SMU SEDERAJAT DAN DIBAWAHNYA
('', 'SMA / SMU', '-'),
('', 'SMEA', '-'),
('', 'STM', '-'),
('', 'SMKK', '-'),
('', 'SPSA', '-'),
('', 'SMTP', '-'),
('', 'SD Kebawah', '-'),
('', 'SMTA Lainnya', '-')