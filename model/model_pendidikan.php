<?php

// CLASS MODEL PENDUDUK
class model_pendidikan extends database
{
	// DIGUNAKAN UNTUK MENJADI OBJEK SAAT INSTANSIASI DI SINI


	// METHOD
	// FUNCTION __CONSTRUCT UNTUK MENANGANI INSTANSIASI CLASS DARI MODEL 
	function __construct()
	{
		// INSTANSIASI CLASS KONEKSI 
		parent::__construct();
	}

	// QUERY UNTUK MENAMPILKAN DATA (SELECT)
	function datapendidikan()
	{
		$koneksi = $this->koneksi;
		// SQL
		$query			= "SELECT * FROM pendidikan";

		$sql			= mysqli_query($koneksi, $query);

		return $sql;
	}


	// QUERY UNTUK MEMASUKKAN DATA (INSERT)
	function dataInsert_pendidikan($kualifikasi, $keterangan)
	{
		$koneksi = $this->koneksi;

		// SQL
		$query		= "INSERT INTO pendidikan VALUES
							   ('','$kualifikasi','$keterangan')";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	// QUERY UNTUK MENGUBAH DATA (UPDATE)
	function dataUpdate_pendidikan($id, $kualifikasi, $keterangan)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query		= "UPDATE pendidikan SET
								kualifikasi			= '$kualifikasi',
								keterangan 		= '$keterangan'
							   WHERE id 	= '$id'
							   ";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	// QUERY UNTUK MENGHAPUS DATA (DELETE)
	function dataDeletependidikan($id)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query		= "DELETE FROM pendidikan
							   WHERE id= '$id'";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
