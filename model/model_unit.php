<?php

// CLASS MODEL PENDUDUK
class model_unit extends database
{
	// DIGUNAKAN UNTUK MENJADI OBJEK SAAT INSTANSIASI DI SINI


	// METHOD
	// FUNCTION __CONSTRUCT UNTUK MENANGANI INSTANSIASI CLASS DARI MODEL 
	function __construct()
	{
		// INSTANSIASI CLASS KONEKSI 
		parent::__construct();
	}

	// QUERY UNTUK MENAMPILKAN DATA (SELECT)
	function dataunit()
	{
		$koneksi = $this->koneksi;
		// SQL
		$query			= "SELECT * FROM unit";

		$sql			= mysqli_query($koneksi, $query);

		return $sql;
	}


	// QUERY UNTUK MEMASUKKAN DATA (INSERT)
	function dataInsert_unit($unit)
	{
		$koneksi = $this->koneksi;
		$id = strtotime("now");
		// SQL
		$query		= "INSERT INTO unit VALUES
							   ('$id','$unit')";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	// QUERY UNTUK MENGUBAH DATA (UPDATE)
	function dataUpdate_unit($id, $unit)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query		= "UPDATE unit SET
								unit			= '$unit'
							   WHERE id 	= '$id'
							   ";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	// QUERY UNTUK MENGHAPUS DATA (DELETE)
	function dataDeleteunit($id)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query		= "DELETE FROM unit
							   WHERE id= '$id'";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
