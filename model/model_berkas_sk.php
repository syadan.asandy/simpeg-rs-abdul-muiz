<?php

// CLASS MODEL PENDUDUK
class model_berkas_sk extends database
{
	// DIGUNAKAN UNTUK MENJADI OBJEK SAAT INSTANSIASI DI SINI


	// METHOD
	// FUNCTION __CONSTRUCT UNTUK MENANGANI INSTANSIASI CLASS DARI MODEL 
	function __construct()
	{
		// INSTANSIASI CLASS KONEKSI 
		parent::__construct();
	}


	// QUERY UNTUK MEMASUKKAN DATA (INSERT)
	function dataInsert($nip, $keterangan, $tgl, $src, $tipe, $lokasi, $jenis)
	{
		$koneksi = $this->koneksi;
		// SQL
		$q1 	= mysqli_query($koneksi, "SELECT * FROM berkas_sk ORDER BY id DESC");
		$dt 	= mysqli_fetch_array($q1);
		$id		= $dt['id'] + 1;

		$id_f = $id . "_" . $src;

		if ($tipe == "image/jpeg" || $tipe == "image/png") {

			$query		= "INSERT INTO berkas_sk VALUES
    							   ('$id','$nip','$keterangan','$tgl','$id_f','gambar', '$jenis')";

			$sql		= mysqli_query($koneksi, $query);

			move_uploaded_file($lokasi, "file_sk/" . $id_f);

			// ubah resolusi
			$orig_image = imagecreatefromjpeg("file_sk/" . $id_f);
			$image_info = getimagesize("file_sk/" . $id_f);
			$width_orig  = $image_info[0]; // current width as found in image file
			$height_orig = $image_info[1]; // current height as found in image file
			if ($width_orig < $height_orig) {
				$width = 250; // new image width
				$height = 400; // new image height
			} elseif ($width_orig > $height_orig) {
				$width = 400; // new image width
				$height = 250; // new image height
			}
			$destination_image = imagecreatetruecolor($width, $height);
			imagecopyresampled($destination_image, $orig_image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
			// This will just copy the new image over the original at the same filePath.
			imagejpeg($destination_image, "file_sk/" . $id_f, 100);
		} else {

			try {
				$query		= "INSERT INTO berkas_sk VALUES
    							   ('$id','$nip','$keterangan','$tgl','$id_f','file', '$jenis')";

				$sql		= mysqli_query($koneksi, $query);
			} catch (\Exception $error) {
				print_r($error);
			}

			move_uploaded_file($lokasi, "file_sk/" . $id_f);
		}

		// CEK SQL
		if ($sql == TRUE) {
			header('Location: index.php?controller=berkas_sk&method=select');
			exit;
		} else {
			return FALSE;
		}
	}

	// QUERY UNTUK MENGUBAH DATA (UPDATE)
	function dataUpdate($id, $nip, $keterangan, $src, $tipe, $lokasi, $jenis)
	{
		$koneksi = $this->koneksi;
		if (empty($src)) {
			// SQL
			try {
				$query		= "UPDATE berkas_sk SET
									nip							= '$nip',	
									jenis						= '$jenis',								
									keterangan					= '$keterangan'
								   WHERE id	= '$id'";

				$sql		= mysqli_query($koneksi, $query);
			} catch (\Exception $error) {
				print_r($error);
			}

			// CEK SQL
			if ($sql == TRUE) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {

			$query2  = "SELECT * FROM berkas_sk WHERE id = '$id'";
			$sql2	= mysqli_query($koneksi, $query2);
			$dt  	= mysqli_fetch_array($sql2);
			if ($dt['src'] == "") {
				if ($tipe == "image/jpeg" || $tipe == "image/png") {
					$q1 	= mysqli_query($koneksi, "SELECT * FROM berkas_sk ORDER BY id DESC");
					$dtx 	= mysqli_fetch_array($q1);
					$idx	= $dtx['id'] + 1;

					$id_f = $idx . "_" . 'sk' . '_' . $src;

					move_uploaded_file($lokasi, "file_sk/" . $id_f);

					// ubah resolusi
					$orig_image = imagecreatefromjpeg("file_sk/" . $id_f);
					$image_info = getimagesize("file_sk/" . $id_f);
					$width_orig  = $image_info[0]; // current width as found in image file
					$height_orig = $image_info[1]; // current height as found in image file
					if ($width_orig < $height_orig) {
						$width = 250; // new image width
						$height = 400; // new image height
					} elseif ($width_orig > $height_orig) {
						$width = 400; // new image width
						$height = 250; // new image height
					}
					$destination_image = imagecreatetruecolor($width, $height);
					imagecopyresampled($destination_image, $orig_image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
					// This will just copy the new image over the original at the same filePath.
					imagejpeg($destination_image, "file_sk/" . $id_f, 100);
				} else {
					$q1 	= mysqli_query($koneksi, "SELECT * FROM berkas_sk ORDER BY id DESC");
					$dtx 	= mysqli_fetch_array($q1);
					$idx		= $dtx['id'] + 1;

					$id_f = $idx . "_" . $src;

					move_uploaded_file($lokasi, "file_sk/" . $id_f);
				}
			} else {
				//nama field foto
				$tag		= $dt['src'];
				// alamat folder src
				$hapus		= "file_sk/$tag";
				// script hapus gambar dari foleder

				unlink($hapus);
				// simpan src baru
				$q1 	= mysqli_query($koneksi, "SELECT * FROM berkas_sk WHERE id = '$id'");
				$dtx 	= mysqli_fetch_array($q1);
				$idx	= $dtx['id'];

				$id_f = $idx . "_" . $src;
				move_uploaded_file($lokasi, "file_sk/" . $id_f);
				if ($tipe == "image/jpeg" || $tipe == "image/png") {
					// ubah resolusi
					$orig_image = imagecreatefromjpeg("file_sk/" . $id_f);
					$image_info = getimagesize("file_sk/" . $id_f);
					$width_orig  = $image_info[0]; // current width as found in image file
					$height_orig = $image_info[1]; // current height as found in image file
					if ($width_orig < $height_orig) {
						$width = 250; // new image width
						$height = 400; // new image height
					} elseif ($width_orig > $height_orig) {
						$width = 400; // new image width
						$height = 250; // new image height
					}
					$destination_image = imagecreatetruecolor($width, $height);
					imagecopyresampled($destination_image, $orig_image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
					// This will just copy the new image over the original at the same filePath.
					imagejpeg($destination_image, "file_sk/" . $id_f, 100);


					//SQL
					try {
						$query3 	= "UPDATE berkas_sk SET
									nip							= '$nip',
									jenis						= '$jenis',
									keterangan					= '$keterangan',
									src 						= '$id_f',
									tipe                        = 'gambar'
								   WHERE id	= '$id'";
						$sql		= mysqli_query($koneksi, $query3);
					} catch (\Exception $error) {
						$error = mysqli_error($koneksi);
					}
				} else {
					//SQL
					try {

						$query3 	= "UPDATE berkas_sk SET
									nip							= '$nip',
									jenis						= '$jenis',
									keterangan					= '$keterangan',
									src 						= '$id_f',
									tipe                        = 'file'
								   WHERE id	= '$id'";
						$sql		= mysqli_query($koneksi, $query3);
					} catch (\Exception $error) {
						$error = mysqli_error($koneksi);
					}
				}
			}
			// CEK SQL
			if ($sql == TRUE) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	// QUERY UNTUK MENGHAPUS DATA (DELETE)
	function dataDelete($nip, $id)
	{
		$koneksi = $this->koneksi;
		// SQL

		$query2  = "SELECT * FROM berkas_sk WHERE id = '$id'";
		$sql2	= mysqli_query($koneksi, $query2);
		$dt  	= mysqli_fetch_array($sql2);

		if ($dt['src'] == "") {
		} else {
			//nama field foto
			$tag		= $dt['src'];
			// alamat folder foto
			$hapus		= "file_sk/$tag";
			// script hapus gambar dari foleder

			unlink($hapus);
		}
		$query		= "DELETE FROM berkas_sk
							   WHERE id ='$id'";



		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
