<?php

// CLASS MODEL PENDUDUK
class model_pegawai extends database
{
	// DIGUNAKAN UNTUK MENJADI OBJEK SAAT INSTANSIASI DI SINI


	// METHOD
	// FUNCTION __CONSTRUCT UNTUK MENANGANI INSTANSIASI CLASS DARI MODEL 
	function __construct()
	{
		// INSTANSIASI CLASS KONEKSI 
		parent::__construct();
	}

	// QUERY UNTUK CEK NIP YANG TELAH DIPAKAI
	function cekNip($nip)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query			= "SELECT nip FROM pegawai 
							   		ORDER BY id ASC";

		$sql			= mysqli_query($koneksi, $query);

		foreach ($sql as $key => $value) {
			if ($nip == $value['nip']) {
				return true;
			}
		}

		return false;
	}

	// QUERY UNTUK MEMASUKKAN DATA (INSERT)
	function dataInsertKualifikasi($nip, $kualifikasi, $status, $jabatan)
	{
		$koneksi = $this->koneksi;
		// SQL

		$asc			= "SELECT id FROM pegawai 
							   		WHERE nip='$nip'";

		$cek_sql		= mysqli_query($koneksi, $asc);
		$pegawai		= mysqli_fetch_array($cek_sql);
		$id_pegawai = $pegawai['id'];
		// if ($jabatan == "Dokter" || $jabatan == "Perawat" || $jabatan == "Apoteker") {
		if (isset($jabatan)) {
			if ($status == 'update' || $status == 'delete') {
				$query = "DELETE FROM tb_pegawai_kualifikasi WHERE pegawai_id = '$id_pegawai'";
				$sql		= mysqli_query($koneksi, $query);
			}
			if ($status == 'delete') {
				return;
			}
			for ($i=0; $i < count($kualifikasi); $i++) { 
				$query		= "INSERT INTO tb_pegawai_kualifikasi VALUES		
	
								   ('','$kualifikasi[$i]','$id_pegawai')";
	
				$sql		= mysqli_query($koneksi, $query);	
			}
		}
		else {
			if ($status == 'update'|| $status == 'delete') {
				$query = "DELETE FROM tb_pegawai_kualifikasi WHERE pegawai_id = '$id_pegawai'";
				$sql		= mysqli_query($koneksi, $query);
			}
			return;
		}

		print_r(mysqli_error($koneksi));
	}

	function dataDetailKualifikasi($nip)
	{
		$koneksi = $this->koneksi;
		// SQL

		$asc			= "SELECT id FROM pegawai 
							   		WHERE nip='$nip'";

		$cek_sql		= mysqli_query($koneksi, $asc);
		$pegawai		= mysqli_fetch_array($cek_sql);
		$id = $pegawai['id'];
		
		$query = "SELECT kualifikasi FROM tb_pegawai_kualifikasi 
					JOIN pendidikan ON pendidikan.id = tb_pegawai_kualifikasi.kualifikasi_id
					WHERE pegawai_id='$id'";
		
		$cek_sqll = mysqli_query($koneksi, $query);
		
		return $cek_sqll;
	}

	// QUERY UNTUK MENAMPILKAN DATA (SELECT)
	function dataSelect($jenis)
	{
		$koneksi = $this->koneksi;
		// SQL
		if ($jenis == 'semua') {
			$query			= "SELECT * FROM pegawai 
										ORDER BY id ASC";
		}
		else {
			$query			= "SELECT * FROM pegawai 
										WHERE jenis_pegawai = '$jenis'
										ORDER BY id ASC";
		}

		$sql			= mysqli_query($koneksi, $query);

		return $sql;
	}

	// QUERY UNTUK MENAMPILKAN DATA (SELECT)
	function dataDetail($nip)
	{
		$koneksi = $this->koneksi;
		// SQL		
		$query			= "SELECT * FROM pegawai WHERE nip ='$nip'";

		$sql			= mysqli_query($koneksi, $query);
		return $sql;
	}

	// QUERY UNTUK MENAMPILKAN DATA (SELECT)
	function dataKeluarga($nip)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query			= "SELECT * FROM keluarga WHERE nip ='$nip'";

		$sql			= mysqli_query($koneksi, $query);

		return $sql;
	}

	// QUERY UNTUK MENAMPILKAN DATA (SELECT)
	function dataAnak($nip)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query			= "SELECT * FROM anak WHERE nip ='$nip'";

		$sql			= mysqli_query($koneksi, $query);

		return $sql;
	}
	// QUERY UNTUK MENAMPILKAN DATA (SELECT)
	function dataGaji($nip)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query			= "SELECT * FROM gaji WHERE nip ='$nip'";

		$sql			= mysqli_query($koneksi, $query);

		return $sql;
	}

	// QUERY UNTUK MENAMPILKAN DATA (SELECT)
	function dataBerkas($nip)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query			= "SELECT * FROM berkas WHERE nip ='$nip' ORDER BY id DESC";

		$sql			= mysqli_query($koneksi, $query);

		return $sql;
	}


	// QUERY UNTUK MEMASUKKAN DATA (INSERT)
	function dataInsert($nip, $nama, $tempat_lahir, $tgl_lahir, $gender, $agama, $kebangsaan, $jumlah_keluarga, $alamat, $lokasi, $sk_terakhir, $pangkat, $tmt_cpns, $tmt,  $jenis, $tmt_capeg, $status, $jabatan, $gaji_pokok, $npwp, $rt, $rw, $desa, $kecamatan, $kabupaten, $kodepos, $wa, $nik, $str, $sip, $unit, $bpjs)
	{
		$koneksi = $this->koneksi;
		// SQL

		$asc			= "SELECT * FROM pegawai 
							   		ORDER BY id DESC";

		$cek_sql		= mysqli_query($koneksi, $asc);
		$idx 			= mysqli_fetch_array($cek_sql);
		$id 			= $idx['id'] + 1;

		if (!empty($sk_terakhir)) {
			move_uploaded_file($lokasi, "sk_terakhir/" . $sk_terakhir);
		}

		$query		= "INSERT INTO pegawai VALUES		

							   ('$id','$nip', '$nama', '$tempat_lahir', '$tgl_lahir', '$gender', '$agama', '$kebangsaan', '$jumlah_keluarga', '$alamat', '$sk_terakhir', '$pangkat', '$tmt_cpns', '$tmt',  '$jenis', '$tmt_capeg', '$status', '$jabatan', '$gaji_pokok', '$npwp', '$rt', '$rw', '$desa', '$kecamatan', '$kabupaten', '$kodepos', '$wa', '$nik', '$str', '$sip', '$unit', '$bpjs')";

		$sql		= mysqli_query($koneksi, $query);

		print_r(mysqli_error($koneksi));

		// $mutasi 	= "INSERT INTO mutasi VALUES
		// 						('$nip','','','','','','','','')";
		// $sql_mutasi	= mysqli_query($koneksi, $mutasi);


		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
		// CEK SQL
		// if ($sql_mutasi == TRUE) {
		// 	return TRUE;
		// } else {
		// 	return FALSE;
		// }
	}

	// QUERY UNTUK MENGUBAH DATA (UPDATE)
	function dataUpdate($nip, $nama, $tempat_lahir, $tgl_lahir, $gender, $agama, $kebangsaan, $jumlah_keluarga, $alamat, $lokasi, $sk_terakhir, $pangkat, $tmt_cpns, $tmt,  $jenis, $tmt_capeg, $status, $jabatan, $gaji_pokok, $npwp, $rt, $rw, $desa, $kecamatan, $kabupaten, $kodepos, $wa, $nik, $str, $sip, $unit, $bpjs)
	{
		$koneksi = $this->koneksi;
		// SQL
		if (empty($sk_terakhir)) {
			$query		= "UPDATE pegawai SET
								nama						= '$nama',
								tempat_lahir 				= '$tempat_lahir',
								tgl_lahir 					= '$tgl_lahir',
								gender 						= '$gender',
								agama 						= '$agama',
								kebangsaan 					= '$kebangsaan',
								jumlah_keluarga				= '$jumlah_keluarga',
								alamat						= '$alamat',																
								pangkat 					= '$pangkat',
								tmt_cpns					= '$tmt_cpns',
								tmt							= '$tmt',
								jenis_pegawai 				= '$jenis',
								tmt_capeg 					= '$tmt_capeg',
								status_pegawai 				= '$status',
								jabatan 					= '$jabatan',								
								gaji_pokok					= '$gaji_pokok',								
								npwp 						= '$npwp',
								rt 							= '$rt',
								rw 							= '$rw',
								desa 						= '$desa',
								kecamatan 					= '$kecamatan',
								kabupaten 					= '$kabupaten',
								kodepos						= '$kodepos',
								wa                          = '$wa',
								nik							= '$nik',
								str							= '$str' ,
								sip							= '$sip' ,
								unit						= '$unit' ,
								bpjs						= '$bpjs'
							   WHERE nip	= '$nip'
							   ";

			$sql		= mysqli_query($koneksi, $query);
			if ($sql == TRUE) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {

			$query2 = "SELECT * from pegawai WHERE nip = '$nip'";

			$sql1 = mysqli_query($koneksi, $query2);
			$dt = mysqli_fetch_array($sql1);

			$tag		= $dt['sk_terakhir'];
			// alamat folder src
			$hapus		= "sk_terakhir/$tag";
			// script hapus gambar dari foleder

			unlink($hapus);

			move_uploaded_file($lokasi, "sk_terakhir/" . $sk_terakhir);


			$query		= "UPDATE pegawai SET
								nama						= '$nama',
								tempat_lahir 				= '$tempat_lahir',
								tgl_lahir 					= '$tgl_lahir',
								gender 						= '$gender',
								agama 						= '$agama',
								kebangsaan 					= '$kebangsaan',
								jumlah_keluarga				= '$jumlah_keluarga',
								alamat						= '$alamat',
								sk_terakhir 				= '$sk_terakhir',								
								pangkat 					= '$pangkat',
								tmt_cpns					= '$tmt_cpns',
								tmt							= '$tmt',
								jenis_pegawai 				= '$jenis',
								tmt_capeg 					= '$tmt_capeg',
								status_pegawai 				= '$status',
								jabatan 					= '$jabatan',								
								gaji_pokok					= '$gaji_pokok',								
								npwp 						= '$npwp',
								rt 							= '$rt',
								rw 							= '$rw',
								desa 						= '$desa',
								kecamatan 					= '$kecamatan',
								kabupaten 					= '$kabupaten',
								kodepos						= '$kodepos',
								wa                          = '$wa',
								nik							= '$nik',
								str							= '$str' ,
								sip							= '$sip' ,
								unit						= '$unit' ,
								bpjs						= '$bpjs'
							   WHERE nip	= '$nip'
							   ";

			$sql		= mysqli_query($koneksi, $query);
			// CEK SQL
			if ($sql == TRUE) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
		print_r(mysqli_error($koneksi));
	}

	// QUERY UNTUK MENGHAPUS DATA (DELETE)
	function dataDelete($nip)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query		= "DELETE FROM pegawai
							   WHERE nip = '$nip'";

		$query2		= "DELETE FROM keluarga
							   WHERE nip = '$nip'";

		$query3		= "DELETE FROM anak
							   WHERE nip = '$nip'";

		$query4		= "DELETE FROM gaji
							   WHERE nip = '$nip'";

		//gambar
		$query_hps  = "SELECT * FROM berkas WHERE nip ='$nip'";
		$sql_hps = mysqli_query($koneksi, $query_hps);
		while ($dt  	= mysqli_fetch_array($sql_hps)) {

			if ($dt['foto'] == "") {
			} else {
				//nama field foto
				$tag		= $dt['foto'];
				// alamat folder foto
				$hapus		= "logo/$tag";
				// script hapus gambar dari foleder

				unlink($hapus);
			}
		}

		$query5		= "DELETE FROM berkas
							   WHERE nip = '$nip'";

		$sql		= mysqli_query($koneksi, $query);
		$sql2		= mysqli_query($koneksi, $query2);
		$sql3		= mysqli_query($koneksi, $query3);
		$sql4		= mysqli_query($koneksi, $query4);
		$sql5		= mysqli_query($koneksi, $query5);


		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
		if ($sql2 == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
		if ($sql3 == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
		if ($sql4 == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
		if ($sql5 == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	//Bagian Keluarga
	// QUERY UNTUK MEMASUKKAN DATA (INSERT)
	function dataInsertKeluarga($nip, $nama, $tempat_lahir, $tgl_lahir, $nik, $pekerjaan, $tgl_perkawinan, $ke, $penghasilan, $telepon)
	{
		$koneksi = $this->koneksi;
		// SQL
		$q1 	= mysqli_query($koneksi, "SELECT * FROM keluarga ORDER BY id DESC");
		$dt 	= mysqli_fetch_array($q1);
		$id		= $dt['id'] + 1;

		$query		= "INSERT INTO keluarga VALUES
							   ('$id','$nip','$nama','$tempat_lahir','$tgl_lahir','$nik','$pekerjaan','$tgl_perkawinan','$ke','$penghasilan','$telepon')";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	// QUERY UNTUK MEMASUKKAN DATA (INSERT)
	function dataUpdateKeluarga($id, $nama, $tempat_lahir, $tgl_lahir, $nik, $pekerjaan, $tgl_perkawinan, $ke, $penghasilan, $telepon)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query		= "UPDATE keluarga SET
								nama		= '$nama',
								tempat 		= '$tempat_lahir',
								tgl_lahir   = '$tgl_lahir',
								nik 		= '$nik',
								pekerjaan 	= '$pekerjaan',
								tgl_nikah 	= '$tgl_perkawinan',
								ke 			= '$ke',
								penghasilan = '$penghasilan',
								telepon 	= '$telepon'
							   WHERE id	= '$id'
							   ";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	// QUERY UNTUK MENGHAPUS DATA (DELETE)
	function dataDeleteKeluarga($id)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query		= "DELETE FROM keluarga
							   WHERE id = '$id'";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	//Bagian Anak
	// QUERY UNTUK MEMASUKKAN DATA (INSERT)
	function dataInsertAnak($nip, $nama, $tempat_lahir, $tgl_lahir, $status, $ke, $gender, $tunjangan, $kawin, $bekerja, $sekolah, $putusan, $telepon)
	{
		$koneksi = $this->koneksi;
		// SQL
		$q1 	= mysqli_query($koneksi, "SELECT * FROM anak ORDER BY id DESC");
		$dt 	= mysqli_fetch_array($q1);
		$id		= $dt['id'] + 1;

		$query		= "INSERT INTO anak VALUES
							   ('$id','$nip','$nama','$tempat_lahir','$tgl_lahir','$status','$ke','$gender','$tunjangan','$kawin','$bekerja','$sekolah','$putusan','$telepon')";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	// QUERY UNTUK MEMASUKKAN DATA (INSERT)
	function dataUbahAnak($id, $nip, $nama, $tempat_lahir, $tgl_lahir, $status, $ke, $gender, $tunjangan, $kawin, $bekerja, $sekolah, $putusan, $telepon)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query		= "UPDATE anak SET
								nama			= '$nama',
								tempat 			= '$tempat_lahir',
								tanggal_lahir   = '$tgl_lahir',
								status 			= '$status',
								ke 				= '$ke',
								gender 			= '$gender',
								tunjangan 		= '$tunjangan',
								kawin 			= '$kawin',
								bekerja 		= '$bekerja',
								sekolah 		= '$sekolah',
								putusan 		= '$putusan',
								telepon			= '$telepon'
							   WHERE id	= '$id'
							   ";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	// QUERY UNTUK MENGHAPUS DATA (DELETE)
	function dataDeleteAnak($id)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query		= "DELETE FROM anak
							   WHERE id = '$id'";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
