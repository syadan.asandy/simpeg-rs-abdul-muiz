<?php

// CLASS MODEL PENDUDUK
class model_mutasi extends database
{
	// DIGUNAKAN UNTUK MENJADI OBJEK SAAT INSTANSIASI DI SINI


	// METHOD
	// FUNCTION __CONSTRUCT UNTUK MENANGANI INSTANSIASI CLASS DARI MODEL 
	function __construct()
	{
		// INSTANSIASI CLASS KONEKSI 
		parent::__construct();
	}

	// QUERY UNTUK MENAMPILKAN DATA (SELECT)
	function dataSelect()
	{
		$koneksi = $this->koneksi;
		// SQL
		$query			= "SELECT * FROM mutasi ORDER BY tgl_keluar ASC";

		$sql			= mysqli_query($koneksi, $query);

		return $sql;
	}

	// QUERY UNTUK MENAMPILKAN DATA (SELECT)
	function dataDetail($nip)
	{
		$koneksi = $this->koneksi;
		// SQL		
		$query			= "SELECT nama,pangkat FROM pegawai WHERE nip ='$nip'";

		$sql			= mysqli_query($koneksi, $query);

		return $sql;
	}

	// CEK APAKAH DATA PEGAWAI ADA TERMUTASI

	function cekData($nip)
	{
		$koneksi = $this->koneksi;

		$query = "SELECT nip FROM mutasi WHERE nip = '$nip'";

		$sql = mysqli_query($koneksi, $query);

		return $sql;
	}

	// QUERY UNTUK MENGUBAH DATA (UPDATE)
	function dataInsertPendidikan($kualifikasi, $nip)
	{
		$koneksi = $this->koneksi;
		// SQL
		// $query = "SELECT * FROM tb_pegawai_kualifikasi";

		for ($i=0; $i < count($kualifikasi); $i++) { 
			$query		= "INSERT INTO tb_mutasi_pendidikan VALUES ('','$kualifikasi[$i]','$nip');";
			$sql		= mysqli_query($koneksi, $query);
		}

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function dataInsert($nip, $nama, $tmt, $jabatan, $alasan, $tgl_keluar, $berkas)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query		= "INSERT INTO mutasi VALUES ('$nip','$nama','$jabatan','$tmt','$tgl_keluar','$alasan', '$berkas');";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function dataUpdate($nip, $pangkat, $tmt_pangkat, $gaji, $tmt_gaji, $pensiun, $tmt_pensiun, $ijasah, $tmt_ijasah)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query		= "UPDATE mutasi SET
								kenaikan_pangkat		= '$pangkat',
								tmt_kenaikan 			= '$tmt_pangkat',
								kenaikan_gaji 			= '$gaji',
								tmt_gaji				= '$tmt_gaji',
								pensiun 				= '$pensiun',
								tmt_pensiun 			= '$tmt_pensiun',
								ijasah 					= '$ijasah',
								tmt_ijasah 				= '$tmt_ijasah'
							   WHERE nip	= '$nip'
							   ";

		$sql		= mysqli_query($koneksi, $query);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	// QUERY UNTUK MENGHAPUS DATA (DELETE)
	function dataDelete($nip)
	{
		$koneksi = $this->koneksi;
		// SQL
		$query		= "DELETE FROM mutasi
							   WHERE nip = '$nip'";

		$sql		= mysqli_query($koneksi, $query);
		$sql		= mysqli_query($koneksi, "DELETE FROM tb_mutasi_pendidikan WHERE mutasi_nip = '$nip'");
		// $sql2		= mysqli_query($koneksi, $query2);
		// $sql3		= mysqli_query($koneksi, $query3);

		// CEK SQL
		if ($sql == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
		// if ($sql2 == TRUE) {
		// 	return TRUE;
		// } else {
		// 	return FALSE;
		// }
		// if ($sql3 == TRUE) {
		// 	return TRUE;
		// } else {
		// 	return FALSE;
		// }
	}
}
