<?php

include("config/koneksi.php");

$nip = $_GET['q'];
$sql = "SELECT id, nip, nama, tmt, jabatan from pegawai where nip='$nip'";

$result = $koneksi->query($sql);

// echo $result->num_rows;
$each = mysqli_fetch_row($result);
?>
    <input type="hidden" name="id" value="<?=$each[0]?>">
                                <tr>
                                    <td width="10%">
                                        <div class="form-group">
                                            Nama
                                        </div>
                                    </td>
                                    <td width="3%">
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input name="nama" class="form-control" placeholder="Nama" autocomplete="off" id="nama" value="<?=$each[2]?>" readonly></input>
                                        </div>
                                    </td>
                                    <td width="3%"></td>
                                    <td>
                                        <div class="form-group">
                                            Tanggal Mulai Kerja
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            :
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group date" id="ex2" data-date="" data-date-format="yyyy-mm-dd" readonly>
                                                <input name="tmt" class="form-control" placeholder="Masukkan Tanggal" value="<?= $each[3] ?>" readonly="readonly">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                    </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        Jabatan Strukural
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        :
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="jabatan" id="jabatan" class="form-control select2" disabled>
                                            <option value=" ">- Pilih Jabatan Struktural -</option>
                                            <option value=" ">Kosong</option>
                                            <?php
                                            include("config/koneksi.php");
                                            $query_jabatan = "select * from jabatan WHERE jenis='jabatan'";
                                            $jabatan = mysqli_query($koneksi, $query_jabatan);
                                            while ($row_jabatan = mysqli_fetch_array($jabatan)) {
                                                // MENAMPILKAN OPSI Kategori
                                                if($row_jabatan['nama'] == $each[4]) {
                                                    echo "<option value='$row_jabatan[nama]' selected>$row_jabatan[nama]</option>";
                                                    echo "<input type='hidden' name='jabatan' value='$row_jabatan[nama]'>";
                                                }
                                                else {
                                                    echo "<option value='$row_jabatan[nama]'>$row_jabatan[nama]</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </td>
                                <td width="3%"></td>
                                <td>
                                    <div class="form-group">
                                        Berkas
                                    </div>
                                </td>
                                <td width="3%">
                                    <div class="form-group">
                                        :
                                    </div>
                                </td>
                                <td width="30%" style="padding-right: 14%">
                                    <div class="form-group">
                                        <input type="file" name="file" accept="application/pdf" class="form-control" required>
                                        <!-- <input type="file" name="file" id=""> -->
                                        <small>*Tipe: pdf</small>
                                    </div>
                                </td>
                            </tr>
                    <!-- baris 9  -->
                            <tr>
                                <td>
                                    <div class="form-group">
                                        Pendidikan
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        :
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <!-- <select name="kualifikasi[]" id="kualifikasi" class="form-control" multiple> -->
                                            <!-- <option value=" ">- Pilih Kualifikasi -</option> -->
                                            <!-- <option value=" ">Kosong</option> -->
                                            <ul>
                                            <?php
                                            include("config/koneksi.php");
                                            $query_kualifikasi = "select * from pendidikan";
                                            $kualifikasi = mysqli_query($koneksi, $query_kualifikasi);
                                            $tb = "select * from tb_pegawai_kualifikasi where pegawai_id='$each[0]'";
                                            $tb_data = mysqli_query($koneksi, $tb);
                                            $array = [];
                                            $i = 0;
                                            while ($row_tb = mysqli_fetch_array($tb_data)) {
                                                array_push($array, $row_tb['kualifikasi_id']);
                                            }
                                            
                                            while ($row_kualifikasi = mysqli_fetch_array($kualifikasi)) {
                                                // MENAMPILKAN OPSI Kategori
                                                if($row_kualifikasi['id'] == $array[$i]) {
                                                    echo "<li>".$row_kualifikasi['kualifikasi']."</li>";
                                                    echo '<input type="hidden" name="kualifikasi[]" value="'.$row_kualifikasi['id'].'">';
                                                    $i++;
                                                }
                                                if ($i == count($array)) {
                                                    break;
                                                }
                                                // echo "<li>".$row_kualifikasi['kualifikasi']."</li>";
                                                // echo "<option value=".$row_kualifikasi['id'].">".$row_kualifikasi['kualifikasi']."</option>";
                                                
                                            }
                                            ?>
                                            </ul>
                                        <!-- </select> -->
                                    </div>
                                </td>
                                <td width="3%"></td>
                                <td>
                                    <div class="form-group">
                                        Alasan
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        :
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <textarea class="form-control" name="alasan" id="alasan" cols="30" rows="5" required></textarea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                    Tanggal Keluar
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        :
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="date" name="tgl_keluar" id="" class="form-control" value="<?= date('Y-m-d')?>">
                                        <!-- <div class="input-group date" id="ex2" data-date="" data-date-format="yyyy-mm-dd">
                                            <input name="tgl_keluar" class="form-control" placeholder="Masukkan Tanggal" value="<?php echo date('Y-m-d'); ?>" readonly="readonly">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </div> -->
                                    </div>
                                </td>
                                <td width="3%"></td>    
                            </tr>
