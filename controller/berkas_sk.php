<?php
// INCLUDE KONEKSI DATABASE 
include("config/database.php");
// INCLUDE MODEL DARI FOLDER MODEL 
include("model/model_berkas_sk.php");
include("model/model_sistem.php");

// CLASS PENDUDUK
class berkas_sk
{
	// PROPERTY
	// DIGUNAKAN UNTUK MENJADI OBJEK SAAT INSTANSIASI DI SINI
	public $berkas_sk;
	public $sistem;

	// METHOD
	// FUNCTION __CONSTRUCT UNTUK MENANGANI INSTANSIASI CLASS DARI MODEL 
	function __construct()
	{
		// INSTANSIASI CLASS MODEL PENDUDUK
		$this->berkas_sk	= new model_berkas_sk();
		$this->sistem	= new model_sistem();
	}

	// FUNCTION UNTUK MENANGANI PROSES SELECT
	function select()
	{
		// MODEL
		$data			        = $this->sistem->dataHome();
		// VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		include "view/dashboard.php";
	}

	// FUNCTION UNTUK MENANGANI PROSES INSERT KE TABEL
	function insert_berkas_sk()
	{
		// DARI VIEW
		// MENAMPUNG DATA YANG DIINPUTKAN
		$nip 			= $_POST['nip'];
		$keterangan		= $_POST['berkas'];
		$tgl 			= $_POST['tgl'];

		$fotox   				= $_FILES['gambar']['name'];
		$lokasi 				= $_FILES['gambar']['tmp_name'];
		$tipe                   = $_FILES['gambar']['type'];

		$src 					= $nip . '_' . 'sk' . '_' . $fotox;

		$jenis 					= $_POST['jenis'];

		if ($tipe == "image/png") {
			echo "<script> 
							  alert('Tambah lampiran gagal, File PNG tidak Diijinkan');
							  window.location = 'index.php?controller=berkas_sk&method=select'; 
							  </script>";
		} else {
			// DARI MODEL
			// MENGARAH KE METHOD DI CLASS MODEL PENDUDUK
			$data			= $this->berkas_sk->dataInsert($nip, $keterangan, $tgl, $src, $tipe, $lokasi, $jenis);

			// DARI VIEW
			// MENGARAHKAN KE FILE VIEW/SELECT.PHP
			// JIKA HASIL PROSES INSERT BERHASIL
			if ($data == TRUE) {
				echo "<script> 
							  window.location = 'index.php?controller=berkas_sk&method=select'; 
							  </script>";
			}
			// MENGARAHKAN KE FILE VIEW/INSERT.PHP
			// JIKA HASIL PROSES INSERT GAGAL
			else {
				echo "<script> 
							  alert('Proses Insert Gagal!');
							  window.location = 'index.php?controller=berkas_sk&method=select'; 
							  </script>";
			}
		}
	}

	// FUNCTION UNTUK MENANGANI PROSES INSERT KE TABEL
	function ubah_berkas()
	{
		// DARI VIEW
		// MENAMPUNG DATA YANG DIINPUTKAN
		$id 			= $_POST['id'];
		$nip 			= $_POST['nip'];
		$keterangan		= $_POST['berkas'];

		$fotox   				= $_FILES['gambar']['name'];
		$lokasi 				= $_FILES['gambar']['tmp_name'];
		$tipe                   = $_FILES['gambar']['type'];
		$jenis					= $_POST['jenis'];

		if ($_FILES['gambar']['name'] == "") {
			$src = null;
		} else {
			$src 					= $nip . '_' . 'sk' . '_' .  $fotox;
		}



		if ($tipe == "image/png") {
			echo "<script> 
							  alert('Tambah lampiran gagal, File PNG tidak Diijinkan');
							  window.location = 'index.php?controller=berkas_sk&method=select'; 
							  </script>";
		} else {
			// DARI MODEL
			// MENGARAH KE METHOD DI CLASS MODEL PENDUDUK
			$data			= $this->berkas_sk->dataUpdate($id, $nip, $keterangan, $src, $tipe, $lokasi, $jenis);

			// DARI VIEW
			// MENGARAHKAN KE FILE VIEW/SELECT.PHP
			// JIKA HASIL PROSES INSERT BERHASIL
			if ($data 		== TRUE) {
				echo "<script> 
							  window.location = 'index.php?controller=berkas_sk&method=select'; 
							  </script>";
			}
			// MENGARAHKAN KE FILE VIEW/INSERT.PHP
			// JIKA HASIL PROSES INSERT GAGAL
			else {
				echo "<script> 
							  alert('Proses Update Gagal!');
							  
							  </script>";
			}
		}
	}

	// FUNCTION UNTUK MENANGANI PROSES DELETE
	function delete()
	{
		// DARI CONTROLLER
		// MENANGKAP NILAI NIK
		$nip			= $_GET['nip'];
		$id 			= $_GET['id'];

		$data = $this->berkas_sk->dataDelete($nip, $id);

		/// DARI VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES DELETE BERHASIL
		if ($data 		== TRUE) {
			echo "<script> 
						  window.location = 'index.php?controller=berkas_sk&method=select'; 
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES DELETE GAGAL
		else {
			echo "<script>
							alert('Proses Delete Gagal!'); 
						  window.location = 'index.php?controller=berkas_sk&method=select'; 
						  </script>";
		}
	}
}
