<?php
// INCLUDE KONEKSI DATABASE 
include("config/database.php");
// INCLUDE MODEL DARI FOLDER MODEL 
include("model/model_pegawai.php");
include("model/model_sistem.php");

// CLASS PENDUDUK
class pegawai
{
	// PROPERTY
	// DIGUNAKAN UNTUK MENJADI OBJEK SAAT INSTANSIASI DI SINI
	public $pegawai;
	public $sistem;

	// METHOD
	// FUNCTION __CONSTRUCT UNTUK MENANGANI INSTANSIASI CLASS DARI MODEL 
	function __construct()
	{
		// INSTANSIASI CLASS MODEL PENDUDUK
		$this->pegawai	= new model_pegawai();
		$this->sistem	= new model_sistem();
	}

	// FUNCTION UNTUK MENANGANI PROSES SELECT
	function select()
	{
		// MODEL
		// MENGARAH KE METHOD DI CLASS MODEL AGAMA
		$jenis = $_GET['jenis'];
		$data			        = $this->sistem->dataHome();
		$data_pegawai			= $this->pegawai->dataSelect($jenis);

		// VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		include "view/dashboard.php";
	}

	// FUNCTION UNTUK MENANGANI PROSES DELETE
	function detail()
	{
		$jenis = $_GET['jenis'];
		// DARI CONTROLLER
		// MENANGKAP NILAI NIK
		$nip			= $_GET['nip'];

		$data		 = $this->sistem->dataHome();
		$data_pegawai = $this->pegawai->dataDetail($nip);
		$data_kualifikasi = $this->pegawai->dataDetailKualifikasi($nip);

		include "view/dashboard.php";
	}

	// FUNCTION UNTUK MENANGANI PROSES SELECT
	function insert()
	{
		$jenis = $_GET['jenis'];
		// MODEL
		// MENGARAH KE METHOD DI CLASS MODEL AGAMA
		$data			        = $this->sistem->dataHome();


		// VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		include "view/dashboard.php";
	}

	// FUNCTION UNTUK MENANGANI PROSES SELECT
	function keluarga()
	{
		$jenis = $_GET['jenis'];
		// MODEL
		// MENGARAH KE METHOD DI CLASS MODEL AGAMA
		$nip					= $_GET['nip'];


		$data			        = $this->sistem->dataHome();
		$data_detail   			= $this->pegawai->dataDetail($nip);
		$data_keluarga   		= $this->pegawai->dataKeluarga($nip);

		// VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		include "view/dashboard.php";
	}

	// FUNCTION UNTUK MENANGANI PROSES SELECT
	function anak()
	{
		$jenis = $_GET['jenis'];
		// MODEL
		// MENGARAH KE METHOD DI CLASS MODEL AGAMA
		$nip					= $_GET['nip'];


		$data			        = $this->sistem->dataHome();
		$data_detail   			= $this->pegawai->dataDetail($nip);
		$data_anak   			= $this->pegawai->dataAnak($nip);


		// VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		include "view/dashboard.php";
	}

	// FUNCTION UNTUK MENANGANI PROSES SELECT
	function berkas()
	{
		$jenis = $_GET['jenis'];
		// MODEL
		// MENGARAH KE METHOD DI CLASS MODEL AGAMA
		$nip					= $_GET['nip'];


		$data			        = $this->sistem->dataHome();
		$data_detail   			= $this->pegawai->dataDetail($nip);
		$data_berkas   			= $this->pegawai->dataBerkas($nip);

		// VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		include "view/dashboard.php";
	}

	// FUNCTION UNTUK MENANGANI PROSES SELECT
	function info()
	{
		// MODEL


		$data			        = $this->sistem->dataHome();

		// VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		include "view/dashboard.php";
	}
	// FUNCTION UNTUK MENANGANI PROSES INSERT KE TABEL
	function insert_data()
	{
		if ($this->pegawai->cekNip($_POST['nip'])) {
			echo "<script> 
						  alert('NIP Sudah Dipakai!');
							window.location = 'index.php?controller=pegawai&method=insert'; 						
						  </script>";
		}
		// DARI VIEW
		// MENAMPUNG DATA YANG DIINPUTKAN

		$nip 			= $_POST['nip'];
		$nama			= $_POST['nama'];
		$tempat_lahir 	= $_POST['tempat'];
		$tahun			= $_POST['tahun'];
		$bulan 			= $_POST['bulan'];
		$hari 			= $_POST['hari'];
		$tgl_lahir  	= $tahun . "-" . $bulan . "-" . $hari;
		$gender			= $_POST['gender'];
		$agama 			= $_POST['agama_pegawai'];
		$kebangsaan 	= $_POST['kebangsaan'];
		$jumlah_keluarga = $_POST['jml_keluarga'];
		$alamat 		= $_POST['alamat'];
		$pangkat 		= $_POST['pangkat'];
		// $tmt_golongan	= $_POST['tmkgolstat'] == 1 ? $_POST['tmt_golongan'] : '0000-00-00';
		// $tmt 			= $_POST['tmkstat'] == 1 ? $_POST['tmt'] : '0000-00-00';
		// $tmt_golongan	= '0000-00-00';
		$tmt_cpns		= $_POST['tmt_cpns'];
		$unit 			= $_POST['unit'];
		$bpjs			= $_POST['bpjs'];

		$tmt 			= $_POST['tmt'];
		$jenis 			= $_POST['jenis_pegawai'];
		$tmt_capeg 		= '0000-00-00';
		$status 		= $_POST['status_pegawai'];
		$jabatan		= $_POST['jabatan'];

		$gaji_pokok1 	= $_POST['gaji_pokok'];
		$gaji_pokok 	= str_replace(".", "", $gaji_pokok1);


		$npwp 			= $_POST['npwp'];
		$nik			= $_POST['nik'];
		$str			= $_POST['str'];
		$sip			= $_POST['sip'];
		$rt 			= $_POST['rt'];
		$rw 			= $_POST['rw'];
		$desa 			= $_POST['desa'];
		$kecamatan 		= $_POST['kecamatan'];
		$kabupaten 		= $_POST['kabupaten'];
		$wa             = $_POST['wa'];
		$kodepos 		= $_POST['kodepos'];

		$lokasi 				= $_FILES['gambar']['tmp_name'];
		$tipe                   = $_FILES['gambar']['type'];

		if ($_FILES['gambar'] != null) {
			if ($tipe != "application/pdf") {
				echo "<script> 
						  alert('Tambah lampiran gagal, File berkas harus pdf');	
						  window.location = 'index.php?controller=pegawai&method=insert'; 
						  </script>";
			}
			$sk_terakhir 	= $_POST['nip'] . '_sk_terakhir_' . $_FILES['gambar']['name'];
		}
		else {
			$sk_terakhir = null;
		}
		

		$alamat = $alamat . ' RT. ' . $rt . ' ' . 'RW. ' . ' ' . $rw . ' ' . 'KODE POS. ' . $kodepos;
		// DARI MODEL
		// MENGARAH KE METHOD DI CLASS MODEL PENDUDUK
		$data			= $this->pegawai->dataInsert($nip, $nama, $tempat_lahir, $tgl_lahir, $gender, $agama, $kebangsaan, $jumlah_keluarga, $alamat, $lokasi, $sk_terakhir, $pangkat, $tmt_cpns, $tmt,  $jenis, $tmt_capeg, $status, $jabatan, $gaji_pokok, $npwp, $rt, $rw, $desa, $kecamatan, $kabupaten, $kodepos, $wa, $nik, $str, $sip, $unit, $bpjs);
		
		// DARI VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES INSERT BERHASIL
		if ($data 		== TRUE) {
			if (isset($_POST['kualifikasi'])) {
				$this->pegawai->dataInsertKualifikasi($nip, $_POST['kualifikasi'], 'update', $jabatan);
			}
			echo "<script> 
						alert('Proses Insert Berhasil!'); 
							window.location = 'index.php?controller=pegawai&method=select'; 
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/INSERT.PHP
		// JIKA HASIL PROSES INSERT GAGAL
		else {
			echo "<script> 
						  alert('Proses Insert Gagal!');
							window.location = 'index.php?controller=pegawai&method=insert'; 						
						  </script>";
		}
	}

	// FUNCTION UNTUK MENANGANI PROSES INSERT KE TABEL
	function ubah_data()
	{
		// DARI VIEW
		// MENAMPUNG DATA YANG DIINPUTKAN
		$nip 			= $_POST['nip'];
		$nama			= $_POST['nama'];
		$tempat_lahir 	= $_POST['tempat'];
		$tahun			= $_POST['tahun'];
		$bulan 			= $_POST['bulan'];
		$hari 			= $_POST['hari'];
		$tgl_lahir  	= $tahun . "-" . $bulan . "-" . $hari;
		$gender			= $_POST['gender'];
		$agama 			= $_POST['agama_pegawai'];
		$kebangsaan 	= $_POST['kebangsaan'];
		$jumlah_keluarga = $_POST['jml_keluarga'];
		$alamat 		= $_POST['alamat'];
		$pangkat 		= $_POST['pangkat'];
		// $tmt_golongan	= isset($_POST['tmt_golongan']) ? $_POST['tmt_golongan'] : '0000-00-00';
		$tmt_cpns		= isset($_POST['tmt_cpns']) ? $_POST['tmt_cpns'] : '0000-00-00';
		$unit 			= $_POST['unit'];
		$bpjs			= $_POST['bpjs'];
		
		$tmt 			= isset($_POST['tmt']) ? $_POST['tmt'] : '0000-00-00';
		$jenis 			= $_POST['jenis_pegawai'];
		$tmt_capeg 		= '0000-00-00';
		$status 		= $_POST['status_pegawai'];
		$jabatan		= $_POST['jabatan'];

		$gaji_pokok1 	= $_POST['gaji_pokok'];
		$gaji_pokok 	= str_replace(".", "", $gaji_pokok1);


		$npwp 			= $_POST['npwp'];
		$nik			= $_POST['nik'];
		$str			= $_POST['str'];
		$sip			= $_POST['sip'];
		$rt 			= $_POST['rt'];
		$rw 			= $_POST['rw'];
		$desa 			= $_POST['desa'];
		$kecamatan 		= $_POST['kecamatan'];
		$kabupaten 		= $_POST['kabupaten'];
		$wa             = $_POST['wa'];
		$kodepos 		= $_POST['kodepos'];

		$lokasi 				= $_FILES['gambar']['tmp_name'];
		$tipe                   = $_FILES['gambar']['type'];

		if ($tipe != "application/pdf" && $_FILES['gambar']['type'] != "") {
			echo "<script> 
					  alert('Tambah lampiran gagal, File berkas harus pdf');	
					  window.location = 'index.php?controller=pegawai&method=detail&nip=$nip'; 											
					  </script>";
		}
		if ($_FILES['gambar']['name'] == "") {
			$sk_terakhir = null;
		} else {
			$sk_terakhir 	= $_POST['nip'] . '_sk_terakhir_' . $_FILES['gambar']['name'];
		}

		// DARI MODEL
		// MENGARAH KE METHOD DI CLASS MODEL PENDUDUK
		$data			= $this->pegawai->dataUpdate($nip, $nama, $tempat_lahir, $tgl_lahir, $gender, $agama, $kebangsaan, $jumlah_keluarga, $alamat, $lokasi, $sk_terakhir, $pangkat, $tmt_cpns, $tmt,  $jenis, $tmt_capeg, $status, $jabatan, $gaji_pokok, $npwp, $rt, $rw, $desa, $kecamatan, $kabupaten, $kodepos, $wa, $nik, $str, $sip, $unit, $bpjs);
		if (isset($_POST['kualifikasi'])) {
			$this->pegawai->dataInsertKualifikasi($nip, $_POST['kualifikasi'], 'update', $jabatan);
		}
		// DARI VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES INSERT BERHASIL
		if ($data == TRUE) {
			echo "<script>
			alert('Proses Update Berhasil!'); 
			window.location = 'index.php?controller=pegawai&method=detail&nip=$nip';
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/INSERT.PHP
		// JIKA HASIL PROSES INSERT GAGAL
		else {
			echo "<script> 
						  alert('Proses Update Gagal!');
						  window.location = 'index.php?controller=pegawai&method=detail&nip=$nip';
						  </script>";
		}
	}

	// FUNCTION UNTUK MENANGANI PROSES DELETE
	function delete()
	{
		$jenis_pegawai = $_GET['jenis'];
		// DARI CONTROLLER
		// MENANGKAP NILAI NIK
		$nip			= $_GET['nip'];

		$this->pegawai->dataInsertKualifikasi($nip, null, 'delete', null);
		$data = $this->pegawai->dataDelete($nip);
		/// DARI VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES DELETE BERHASIL
		if ($data 		== TRUE) {
			echo "<script> 
					alert('Proses Delete Berhasil!'); 
						  window.location = 'index.php?controller=pegawai&method=select&jenis=".$jenis_pegawai."'; 
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES DELETE GAGAL
		else {
			echo "<script>
							alert('Proses Delete Gagal!'); 
						  window.location = 'index.php?controller=pegawai&method=select&jenis=".$jenis_pegawai."'; 
						  </script>";
		}
	}

	// Bagian Keluarga
	// FUNCTION UNTUK MENANGANI PROSES INSERT KE TABEL
	function insert_keluarga()
	{
		// DARI VIEW
		// MENAMPUNG DATA YANG DIINPUTKAN
		$nip 			= $_POST['nip'];
		$nama			= $_POST['nama'];
		$tempat_lahir 	= $_POST['tempat'];
		$tahun			= $_POST['tahun'];
		$bulan 			= $_POST['bulan'];
		$hari 			= $_POST['hari'];
		$tgl_lahir  	= $tahun . "-" . $bulan . "-" . $hari;
		$pekerjaan 		= $_POST['pekerjaan'];
		$tgl_perkawinan = $_POST['tgl_perkawinan'];
		$ke				= $_POST['ke'];
		$nik 		    = $_POST['nik'];
		$penghasilan	= $_POST['penghasilan'];
		$telepon		= $_POST['telepon'];

		// DARI MODEL
		// MENGARAH KE METHOD DI CLASS MODEL PENDUDUK
		$data_insert			= $this->pegawai->dataInsertKeluarga($nip, $nama, $tempat_lahir, $tgl_lahir, $nik, $pekerjaan, $tgl_perkawinan, $ke, $penghasilan, $telepon);

		// DARI VIEW
		if ($data_insert 		== TRUE) {
			echo "<script> 
						  window.location = 'index.php?controller=pegawai&method=keluarga&nip=$nip';
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/INSERT.PHP
		// JIKA HASIL PROSES INSERT GAGAL
		else {
			echo "<script> 
						  alert('Proses Insert Gagal!');
						  window.location = 'index.php?controller=pegawai&method=keluarga&nip='.$nip; 
						  </script>";
		}
	}

	// FUNCTION UNTUK MENANGANI PROSES Update
	function ubah_keluarga()
	{
		// DARI VIEW
		// MENAMPUNG DATA YANG DIINPUTKAN
		$id				= $_POST['id'];
		$nip 			= $_POST['nip'];
		$nama			= $_POST['nama'];
		$tempat_lahir 	= $_POST['tempat'];
		$tahun			= $_POST['tahun'];
		$bulan 			= $_POST['bulan'];
		$hari 			= $_POST['hari'];
		$tgl_lahir  	= $tahun . "-" . $bulan . "-" . $hari;
		$pekerjaan 		= $_POST['pekerjaan'];
		$tgl_perkawinan = $_POST['tgl_perkawinan'];
		$ke				= $_POST['ke'];
		$nik 		    = $_POST['nik'];
		$penghasilan	= $_POST['penghasilan'];
		$telepon		= $_POST['telepon'];

		// DARI MODEL
		// MENGARAH KE METHOD DI CLASS MODEL PENDUDUK
		$data_insert			= $this->pegawai->dataUpdateKeluarga($id, $nama, $tempat_lahir, $tgl_lahir, $nik, $pekerjaan, $tgl_perkawinan, $ke, $penghasilan, $telepon);

		// DARI VIEW
		if ($data_insert 		== TRUE) {
			echo "<script> 
						  window.location = 'index.php?controller=pegawai&method=keluarga&nip=$nip';
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/INSERT.PHP
		// JIKA HASIL PROSES INSERT GAGAL
		else {
			echo "<script> 
						  alert('Proses Update Gagal!');
						  window.location = 'index.php?controller=pegawai&method=keluarga&nip=$nip'; 
						  </script>";
		}
	}
	// FUNCTION UNTUK MENANGANI PROSES DELETE
	function delete_keluarga()
	{
		// DARI CONTROLLER
		// MENANGKAP NILAI NIK
		$id			= $_GET['id'];
		$nip		= $_GET['nip'];

		$data = $this->pegawai->dataDeleteKeluarga($id);

		/// DARI VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES DELETE BERHASIL
		if ($data 		== TRUE) {
			echo "<script> 
						  window.location = 'index.php?controller=pegawai&method=keluarga&nip=$nip'; 
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES DELETE GAGAL
		else {
			echo "<script>
							alert('Proses Delete Gagal!'); 
						  window.location = 'index.php?controller=pegawai&method=keluarga&nip=$nip'; 
						  </script>";
		}
	}



	// Bagian Keluarga
	// FUNCTION UNTUK MENANGANI PROSES INSERT KE TABEL
	function insert_anak()
	{
		// DARI VIEW
		// MENAMPUNG DATA YANG DIINPUTKAN
		$nip 			= $_POST['nip'];
		$nama			= $_POST['nama'];
		$tempat_lahir 	= $_POST['tempat'];
		$tahun			= $_POST['tahun'];
		$bulan 			= $_POST['bulan'];
		$hari 			= $_POST['tgl'];
		$tgl_lahir  	= $tahun . "-" . $bulan . "-" . $hari;
		$kawin			= $_POST['kawin'];
		$tunjangan		= $_POST['tunjangan'];
		$ke				= $_POST['ke'];
		$status 		= $_POST['status'];
		$bekerja 		= $_POST['bekerja'];
		$sekolah		= $_POST['sekolah'];
		$putusan 		= $_POST['putusan'];
		$gender 		= $_POST['gender'];
		$telepon 		= $_POST['telepon'];

		// DARI MODEL
		// MENGARAH KE METHOD DI CLASS MODEL PENDUDUK
		$data_insert			= $this->pegawai->dataInsertAnak($nip, $nama, $tempat_lahir, $tgl_lahir, $status, $ke, $gender, $tunjangan, $kawin, $bekerja, $sekolah, $putusan, $telepon);

		// DARI VIEW
		if ($data_insert 		== TRUE) {
			echo "<script> 
						  window.location = 'index.php?controller=pegawai&method=anak&nip=$nip';
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/INSERT.PHP
		// JIKA HASIL PROSES INSERT GAGAL
		else {
			echo "<script> 
						  alert('Proses Insert Gagal!');
						  window.location = 'index.php?controller=pegawai&method=anak&nip='.$nip; 
						  </script>";
		}
	}

	// FUNCTION UNTUK MENANGANI PROSES Update
	function ubah_anak()
	{
		// DARI VIEW
		// MENAMPUNG DATA YANG DIINPUTKAN
		$id				= $_POST['id'];
		$nip 			= $_POST['nip'];
		$nama			= $_POST['nama'];
		$tempat_lahir 	= $_POST['tempat'];
		$tahun			= $_POST['tahun'];
		$bulan 			= $_POST['bulan'];
		$hari 			= $_POST['tgl'];
		$tgl_lahir  	= $tahun . "-" . $bulan . "-" . $hari;
		$kawin			= $_POST['kawin'];
		$tunjangan		= $_POST['tunjangan'];
		$ke				= $_POST['ke'];
		$status 		= $_POST['status'];
		$bekerja 		= $_POST['bekerja'];
		$sekolah		= $_POST['sekolah'];
		$putusan 		= $_POST['putusan'];
		$gender 		= $_POST['gender'];
		$telepon 		= $_POST['telepon'];

		// DARI MODEL
		// MENGARAH KE METHOD DI CLASS MODEL PENDUDUK
		$data_insert			= $this->pegawai->dataUbahAnak($id, $nip, $nama, $tempat_lahir, $tgl_lahir, $status, $ke, $gender, $tunjangan, $kawin, $bekerja, $sekolah, $putusan, $telepon);

		// DARI VIEW
		if ($data_insert 		== TRUE) {
			echo "<script> 
						  window.location = 'index.php?controller=pegawai&method=anak&nip=$nip';
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/INSERT.PHP
		// JIKA HASIL PROSES INSERT GAGAL
		else {
			echo "<script> 
						  alert('Proses Update Gagal!');
						  window.location = 'index.php?controller=pegawai&method=anak&nip=$nip'; 
						  </script>";
		}
	}
	// FUNCTION UNTUK MENANGANI PROSES DELETE
	function delete_anak()
	{
		// DARI CONTROLLER
		// MENANGKAP NILAI NIK
		$id			= $_GET['id'];
		$nip		= $_GET['nip'];

		$data = $this->pegawai->dataDeleteAnak($id);

		/// DARI VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES DELETE BERHASIL
		if ($data 		== TRUE) {
			echo "<script> 
						  window.location = 'index.php?controller=pegawai&method=anak&nip=$nip'; 
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES DELETE GAGAL
		else {
			echo "<script>
							alert('Proses Delete Gagal!'); 
						  window.location = 'index.php?controller=pegawai&method=anak&nip=$nip'; 
						  </script>";
		}
	}
}
