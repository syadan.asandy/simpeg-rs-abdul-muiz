<?php
// INCLUDE KONEKSI DATABASE 
include("config/database.php");
// INCLUDE MODEL DARI FOLDER MODEL 
include("model/model_pendidikan.php");
include("model/model_sistem.php");

// CLASS PENDUDUK
class pendidikan
{
	// PROPERTY
	// DIGUNAKAN UNTUK MENJADI OBJEK SAAT INSTANSIASI DI SINI
	public $pendidikan;
	public $sistem;

	// METHOD
	// FUNCTION __CONSTRUCT UNTUK MENANGANI INSTANSIASI CLASS DARI MODEL 
	function __construct()
	{
		// INSTANSIASI CLASS MODEL PENDUDUK
		$this->pendidikan	= new model_pendidikan();
		$this->sistem	= new model_sistem();
	}

	// FUNCTION UNTUK MENANGANI PROSES SELECT
	function pendidikan()
	{
		// MODEL
		// MENGARAH KE METHOD DI CLASS MODEL AGAMA
		$data			        = $this->sistem->dataHome();
		$data_pendidikan			= $this->pendidikan->datapendidikan();


		// VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		include "view/dashboard.php";
	}

	// FUNCTION UNTUK MENANGANI PROSES INSERT KE TABEL
	function insert_pendidikan()
	{
		// DARI VIEW
		// MENAMPUNG DATA YANG DIINPUTKAN
		$keterangan 	= $_POST['keterangan'];
		$kualifikasi			= $_POST['kualifikasi'];


		// DARI MODEL
		// MENGARAH KE METHOD DI CLASS MODEL PENDUDUK
		$data			= $this->pendidikan->dataInsert_pendidikan($kualifikasi, $keterangan);

		// DARI VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES INSERT BERHASIL
		if ($data 		== TRUE) {
			echo "<script>
						  window.location = 'index.php?controller=pendidikan&method=pendidikan'; 
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/INSERT.PHP
		// JIKA HASIL PROSES INSERT GAGAL
		else {
			echo "<script> 
						  alert('Proses Insert Gagal!');
						  window.location = 'index.php?controller=pendidikan&method=pendidikan'; 
						  </script>";
		}
	}

	// FUNCTION UNTUK MENANGANI PROSES INSERT KE TABEL
	function update_pendidikan()
	{
		// DARI CONTROLLER
		// MENAMPUNG DATA YANG DIUBAH
		$id				= $_POST['id'];
		$keterangan 	= $_POST['keterangan'];
		$kualifikasi			= $_POST['kualifikasi'];


		// DARI MODEL
		// MENGARAH KE METHOD DI CLASS MODEL PENDUDUK
		$data			= $this->pendidikan->dataUpdate_pendidikan($id, $kualifikasi, $keterangan);

		// DARI VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES UPDATE BERHASIL
		if ($data 		== TRUE) {
			echo "<script> 
						  window.location = 'index.php?controller=pendidikan&method=pendidikan'; 
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/UPDATE.PHP
		// JIKA HASIL PROSES UPDATE GAGAL
		else {
			echo "<script> 
						  alert('Proses Update Gagal!');
						  window.location = 'index.php?controller=pendidikan&method=pendidikan'; 
						  </script>";
		}
	}

	// FUNCTION UNTUK MENANGANI PROSES DELETE
	function delete_pendidikan()
	{
		// DARI CONTROLLER
		// MENANGKAP NILAI NIK
		$id		= $_GET['id'];

		$data = $this->pendidikan->dataDeletependidikan($id);

		/// DARI VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES DELETE BERHASIL
		if ($data 		== TRUE) {
			echo "<script> 
						  alert('Proses Delete Berhasil!');
						  window.location = 'index.php?controller=pendidikan&method=pendidikan'; 
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES DELETE GAGAL
		else {
			echo "<script>
							alert('Proses Delete Gagal!'); 
						  window.location = 'index.php?controller=pendidikan&method=pendidikan'; 
						  </script>";
		}
	}
}
