<?php
// INCLUDE KONEKSI DATABASE 
include("config/database.php");
// INCLUDE MODEL DARI FOLDER MODEL 
include("model/model_unit.php");
include("model/model_sistem.php");

// CLASS PENDUDUK
class unit
{
	// PROPERTY
	// DIGUNAKAN UNTUK MENJADI OBJEK SAAT INSTANSIASI DI SINI
	public $unit;
	public $sistem;

	// METHOD
	// FUNCTION __CONSTRUCT UNTUK MENANGANI INSTANSIASI CLASS DARI MODEL 
	function __construct()
	{
		// INSTANSIASI CLASS MODEL PENDUDUK
		$this->unit	= new model_unit();
		$this->sistem	= new model_sistem();
	}

	// FUNCTION UNTUK MENANGANI PROSES SELECT
	function unit()
	{
		// MODEL
		// MENGARAH KE METHOD DI CLASS MODEL AGAMA
		$data			        = $this->sistem->dataHome();
		$data_unit			= $this->unit->dataunit();


		// VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		include "view/dashboard.php";
	}

	// FUNCTION UNTUK MENANGANI PROSES INSERT KE TABEL
	function insert_unit()
	{
		// DARI VIEW
		// MENAMPUNG DATA YANG DIINPUTKAN
		$unit 	= $_POST['unit'];


		// DARI MODEL
		// MENGARAH KE METHOD DI CLASS MODEL PENDUDUK
		$data			= $this->unit->dataInsert_unit($unit);

		// DARI VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES INSERT BERHASIL
		if ($data 		== TRUE) {
			echo "<script>
							alert('Proses Insert Berhasil!');
						  window.location = 'index.php?controller=unit&method=unit'; 
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/INSERT.PHP
		// JIKA HASIL PROSES INSERT GAGAL
		else {
			echo "<script> 
						  alert('Proses Insert Gagal!');
						  window.location = 'index.php?controller=unit&method=unit'; 
						  </script>";
		}
	}

	// FUNCTION UNTUK MENANGANI PROSES INSERT KE TABEL
	function update_unit()
	{
		// DARI CONTROLLER
		// MENAMPUNG DATA YANG DIUBAH
		$id				= $_POST['id'];
		$unit 	= $_POST['unit'];


		// DARI MODEL
		// MENGARAH KE METHOD DI CLASS MODEL PENDUDUK
		$data			= $this->unit->dataUpdate_unit($id, $unit);

		// DARI VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES UPDATE BERHASIL
		if ($data 		== TRUE) {
			echo "<script> 
						alert('Proses Update Berhasil!');
						  window.location = 'index.php?controller=unit&method=unit'; 
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/UPDATE.PHP
		// JIKA HASIL PROSES UPDATE GAGAL
		else {
			echo "<script> 
						  alert('Proses Update Gagal!');
						  window.location = 'index.php?controller=unit&method=unit'; 
						  </script>";
		}
	}

	// FUNCTION UNTUK MENANGANI PROSES DELETE
	function delete_unit()
	{
		// DARI CONTROLLER
		// MENANGKAP NILAI NIK
		$id		= $_GET['id'];

		$data = $this->unit->dataDeleteunit($id);

		/// DARI VIEW
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES DELETE BERHASIL
		if ($data 		== TRUE) {
			echo "<script> 
						  alert('Proses Delete Berhasil!');
						  window.location = 'index.php?controller=unit&method=unit'; 
						  </script>";
		}
		// MENGARAHKAN KE FILE VIEW/SELECT.PHP
		// JIKA HASIL PROSES DELETE GAGAL
		else {
			echo "<script>
							alert('Proses Delete Gagal!'); 
						  window.location = 'index.php?controller=unit&method=unit'; 
						  </script>";
		}
	}
}
