-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2021 at 05:12 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_simpeg`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id` int(100) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `tgl` date NOT NULL,
  `jam_masuk` varchar(50) NOT NULL,
  `jam_keluar` varchar(50) NOT NULL,
  `status` enum('A','I','S','C','X') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id`, `nip`, `tgl`, `jam_masuk`, `jam_keluar`, `status`) VALUES
(1, '196403261987101001', '2019-01-04', '07:50', '16:46', 'A'),
(3, '9967564568943644234', '2019-01-01', '', '', 'C'),
(4, '9967564568943644234', '2019-01-04', '', '', 'S');

-- --------------------------------------------------------

--
-- Table structure for table `anak`
--

CREATE TABLE `anak` (
  `id` int(100) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tempat` text NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `ke` int(10) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `tunjangan` varchar(50) NOT NULL,
  `kawin` varchar(50) NOT NULL,
  `bekerja` varchar(50) NOT NULL,
  `sekolah` varchar(50) NOT NULL,
  `putusan` varchar(100) NOT NULL,
  `telepon` varchar(50) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anak`
--

INSERT INTO `anak` (`id`, `nip`, `nama`, `tempat`, `tanggal_lahir`, `status`, `ke`, `gender`, `tunjangan`, `kawin`, `bekerja`, `sekolah`, `putusan`, `telepon`) VALUES
(1, '197009261997031007', 'ALFATH SHIFA GHIFARA', 'Sungailiat', '1998-09-27', 'AK', 1, 'P', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(2, '197009261997031007', 'KHANSAHATIKAH KHAIRUNNISA', 'Sungailiat', '2003-03-05', 'AK', 1, 'P', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(3, '197009261997031007', 'HANIF MUHAMMAD FALAH', 'Pangkalpinang', '2011-06-17', 'AK', 1, 'L', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(4, '197412042005012002', 'NAJWA WIHDAHANI', 'Pangkalpinang', '2020-01-01', 'AK', 1, 'P', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(5, '197910072003121001', 'QHODARI TRISTAN EL FAQIH', 'Pangkalpinang', '2011-08-22', 'AK', 1, 'L', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(6, '197910072003121001', 'QANITA SHANAZ EL MAULIDA', 'Pangkalpinang', '2013-03-25', 'AK', 1, 'P', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(8, '197405142007011031', 'ARBIAN PUTRA RAMDHAN', 'Sungailiat', '2005-10-31', 'AK', 1, 'L', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(9, '197405142007011031', 'ANNISYAH MAHARANI', 'Sungailiat', '2003-05-05', 'AK', 1, 'Perempuan', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(10, '197405142007011031', 'SASKIA PUTRI MAHARANI', 'Sungailiat', '2012-01-21', 'AK', 1, 'P', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(13, '198304192010012013 ', 'MUHAMMAD AUFA SAMBARA TRESNADI', 'Pangkalpinang', '2012-03-13', 'AK', 1, 'L', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(14, '198304192010012013 ', 'KHANSA KHAIRUNNISA TRESNADI', 'Pangkalpinang', '2014-09-14', 'AK', 1, 'P', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(15, '197807132002121006', 'JOSUA LUBIS', 'Pangkalpinang', '2015-02-02', 'AK', 1, 'L', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(16, '198505042009032008', 'NAFISAH AULIA RACHMANDA', 'Pangkalpinang', '2009-11-13', 'AK', 1, 'P', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(17, '198505042009032008', 'HANIFAH FIRLII RACHMANDA', 'Pangkalpinang', '2011-03-20', 'AK', 1, 'P', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(18, '198505042009032008', 'ARGA ALFATIH RACHMANDA', 'Pangkalpinang', '2015-05-17', 'AK', 1, 'L', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0'),
(19, '19580818.198404.1.002', 'asdsadsad', 'asdhsjkadh', '2008-12-05', 'AK', 1, 'L', 'Dapat', 'Belum', 'Tidak', 'Masih', '', '0821308213');

-- --------------------------------------------------------

--
-- Table structure for table `berkas`
--

CREATE TABLE `berkas` (
  `id` int(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `tgl` date NOT NULL,
  `foto` varchar(255) NOT NULL,
  `tipe` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `berkas_sk`
--

CREATE TABLE `berkas_sk` (
  `id` int(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `tgl` date NOT NULL,
  `src` varchar(255) NOT NULL,
  `tipe` varchar(100) NOT NULL,
  `jenis` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cuti`
--

CREATE TABLE `cuti` (
  `id_cuti` bigint(11) NOT NULL,
  `id_cek` int(10) NOT NULL,
  `tgl_cuti` date NOT NULL,
  `nip` varchar(50) NOT NULL,
  `jenis_cuti` varchar(100) NOT NULL,
  `alasan` text NOT NULL,
  `lama` int(10) NOT NULL,
  `cek` varchar(50) NOT NULL,
  `tgl_a` date NOT NULL,
  `tgl_b` date NOT NULL,
  `nip_atasan` varchar(255) NOT NULL,
  `status` enum('A','X','Y','P','T','B') NOT NULL,
  `n2` int(10) NOT NULL,
  `n1` int(10) NOT NULL,
  `n` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuti`
--

INSERT INTO `cuti` (`id_cuti`, `id_cek`, `tgl_cuti`, `nip`, `jenis_cuti`, `alasan`, `lama`, `cek`, `tgl_a`, `tgl_b`, `nip_atasan`, `status`, `n2`, `n1`, `n`) VALUES
(20190106011, 11, '2019-01-06', '9967564568943644234', 'Cuti Sakit', 'sfrbn', 3, 'Hari', '2019-01-07', '2019-01-09', '9967564568943644234', 'A', 0, 0, 0),
(20190107001, 1, '2019-01-07', '9967564568943644234', 'Cuti Besar', 'ihweg', 2, 'Hari', '2019-01-15', '2019-01-23', '196403261987101001', 'A', 0, 0, 0),
(20190112001, 1, '2019-01-12', '9967564568943644234', 'Cuti Alasan Penting', 'sfrb', 4, 'Bulan', '2019-01-25', '2019-01-28', '196403261987101001', 'Y', 0, 0, 0),
(20190119001, 1, '2019-01-19', '9967564568943644234', 'Cuti Sakit', 'klneb', 3, 'Hari', '2019-01-21', '2019-01-22', '197403081993111002', 'B', 0, 0, 0),
(20190119002, 2, '2019-01-19', '9967564568943644234', 'Cuti Tahunan', 'kjevb', 2, 'Hari', '2019-01-22', '2019-01-24', '196403261987101001', 'B', 0, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `detail_s_ijasah`
--

CREATE TABLE `detail_s_ijasah` (
  `id_detail_s_ijasah` bigint(100) NOT NULL,
  `nomor` varchar(100) NOT NULL,
  `tingkat` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `golongan`
--

CREATE TABLE `golongan` (
  `golongan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `golongan`
--

INSERT INTO `golongan` (`golongan`, `keterangan`) VALUES
('GOL I A', 'SD'),
('GOL I B', 'SMP'),
('GOL II A', 'SMA'),
('Golongan III A', 'Sarjana S1');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `50` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jenis` enum('pangkat','jenis','status','jabatan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`50`, `nama`, `jenis`) VALUES
(1, 'Kepala Dinas', 'jabatan'),
(2, 'sektretaris', 'jabatan'),
(3, 'Pegawai Negeri Sipil', 'jenis'),
(4, 'honorer', 'jenis'),
(5, 'Pengatur Muda / IIa', 'pangkat'),
(6, 'Pengatur Muda Tk I / IIb', 'pangkat'),
(7, 'Aktif', 'status'),
(8, 'Non Aktif', 'status'),
(9, 'Pengatur / IIc', 'pangkat'),
(10, 'Pengatur Tk I /IId', 'pangkat'),
(11, 'Penata Muda / IIIa', 'pangkat'),
(12, 'Penata Muda Tk I / IIIb', 'pangkat'),
(13, 'Penata / IIIc', 'pangkat'),
(14, 'Penata Tk I / IIId', 'pangkat'),
(15, 'Pembina / IVa', 'pangkat'),
(16, 'Pembina Tk I / IVb', 'pangkat'),
(17, 'Pembina Utama Muda / IVc', 'pangkat'),
(18, 'Pembina Utama Madya / IVd', 'pangkat'),
(19, 'Kepala UPTD Laboratorium Lingkungan', 'jabatan'),
(20, 'Kepala Bidang Tata Lingkungan', 'jabatan'),
(21, 'Kepala Bidang Pengendalian dan Penataan Lingkungan Hidup', 'jabatan'),
(22, 'Kepala Bidang PLHPSPKLH', 'jabatan'),
(23, 'Kasubbag Umum', 'jabatan'),
(24, 'Kasubbag Perencaan', 'jabatan'),
(25, 'Kasubbag Keuangan', 'jabatan'),
(26, 'Analis SDM Aparatur', 'jabatan'),
(27, 'Penyusun Kebutuhan Barang Inventaris', 'jabatan'),
(28, 'Pranata Komputer Pelaksana', 'jabatan'),
(29, 'Pengadministrasi Umum', 'jabatan'),
(30, 'Analis Perencanaan, Evaluasi dan Pelaporan', 'jabatan'),
(31, 'Pengelola Program dan Kegiatan', 'jabatan'),
(32, 'Bendahara', 'jabatan'),
(33, 'Pengadministrasi Keuangan', 'jabatan'),
(34, 'Pengendali Dampak Lingkungan Pertama', 'jabatan'),
(35, 'Pengendali Dampak Lingkungan Muda', 'jabatan'),
(36, 'PPLH Pertama', 'jabatan'),
(37, 'PPLH Madya', 'jabatan'),
(38, 'Kasi Perencaan Lingkungan Hidup', 'jabatan'),
(39, 'Kasi Pengendalian Pencemaran dan Kerusakan LIngkungan Hidup', 'jabatan'),
(40, 'Kasi Pemeliharaan Lingkungan Hidup dan Pengelolaan Sampah', 'jabatan'),
(41, 'Pengelola Data', 'jabatan'),
(42, 'Pengelola Lingkungan', 'jabatan'),
(43, 'Kasi Kajian dampak Lingkungan', 'jabatan'),
(44, 'Kasi Penegakan Hukum, Limbah B3, Pengaduan dan penyelesaian Sengketa Lingkungan Hidup', 'jabatan'),
(45, 'Kasi Peningkatan Kapasitas Lingkungan Hidup', 'jabatan'),
(46, 'Penelaah Dampak Lingkungan', 'jabatan'),
(47, 'Pengelola Dokumen Mengenai AMDAL', 'jabatan'),
(48, 'Pengelola Penyelesaian Hasil Pengawasan', 'jabatan'),
(49, 'Pengawas Lingkungan Hidup Pertama', 'jabatan'),
(50, 'Pengawas Lingkungan Hidup Muda', 'jabatan'),
(51, 'Pengawas Lingkungan Hidup Madya', 'jabatan'),
(52, 'Kasubbag Tata Usaha', 'jabatan'),
(53, 'Kasi Pengujian', 'jabatan'),
(54, 'Kasi Pengendalian Mutu', 'jabatan'),
(55, 'Analis Tata Usaha', 'jabatan'),
(56, 'Pengadministrasi Contoh Uji', 'jabatan'),
(57, 'Pengadministrasi Pengujian', 'jabatan'),
(58, 'Analis Laboratorium', 'jabatan'),
(59, 'Pengelola Laboratorium', 'jabatan'),
(60, 'Dokter', 'jabatan'),
(61, 'Perawat', 'jabatan'),
(62, 'Apoteker', 'jabatan'),
(63, 'PPTH', 'jenis');

-- --------------------------------------------------------

--
-- Table structure for table `jml_hari_rekap`
--

CREATE TABLE `jml_hari_rekap` (
  `id_jml` int(10) NOT NULL,
  `tgl` date NOT NULL,
  `jml` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jml_hari_rekap`
--

INSERT INTO `jml_hari_rekap` (`id_jml`, `tgl`, `jml`) VALUES
(1, '2019-01-01', 19);

-- --------------------------------------------------------

--
-- Table structure for table `keluarga`
--

CREATE TABLE `keluarga` (
  `id` int(100) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tempat` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `nik` varchar(50) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `tgl_nikah` date NOT NULL,
  `ke` int(10) NOT NULL,
  `penghasilan` int(10) NOT NULL,
  `telepon` varchar(50) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keluarga`
--

INSERT INTO `keluarga` (`id`, `nip`, `nama`, `tempat`, `tgl_lahir`, `nik`, `pekerjaan`, `tgl_nikah`, `ke`, `penghasilan`, `telepon`) VALUES
(1, '197009261997031007', 'herawati.amd', 'Sungailiat', '1970-03-04', '2147483647', 'Ibu Rumah Tangga', '1997-11-06', 1, 3000000, '0'),
(2, '197412042005012002', 'SYAHRUROZI, A.Md', 'Pangkalpinang', '1972-08-18', '2147483647', 'Swasta', '2012-06-21', 1, 3000000, '0'),
(3, '197910072003121001', 'raden ayu fitria miranti', 'Pangkalpinang', '1980-08-03', '2147483647', 'PNS', '2010-01-27', 1, 2900000, '0'),
(5, '197405142007011031', 'HARYANI', 'Sungailiat', '1977-09-21', '2147483647', 'PNS', '2003-03-02', 1, 3000000, '0'),
(7, '198304192010012013 ', 'AGUNG TRESNADI, ST', 'Jakarta', '1979-06-18', '16707', 'TNI AL', '2006-06-12', 1, 8000000, '0'),
(8, '197807132002121006', 'Dewi olivia Nilapensa Siagian', 'Pangkalpinang', '1990-01-19', '2147483647', 'Guru Swasta', '2013-12-28', 1, 3000000, '0'),
(9, '198505042009032008', 'R.RACHMANDA GUNTUR GENI', 'Semarang', '1979-07-02', '19790701', 'PNS', '2007-10-26', 1, 5000000, '0'),
(10, '19580818.198404.1.002', 'asdsad', 'asdasd', '2021-04-02', '213213123.123.213.1.3', 'asdasd', '2021-01-04', 2, 20000000, '0881238213'),
(11, '19580818.198404.1.002', 'ashdjkashdjh', 'ahsdkjhasdjk', '2010-10-04', '123213.213.213.21.3', 'asdkjsadjnkjasjnd', '2021-02-25', 1, 12321322, '12838217389');

-- --------------------------------------------------------

--
-- Table structure for table `mutasi`
--

CREATE TABLE `mutasi` (
  `nip` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `tmt` date DEFAULT NULL,
  `tgl_keluar` date DEFAULT NULL,
  `alasan` varchar(50) NOT NULL,
  `file` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mutasi`
--

INSERT INTO `mutasi` (`nip`, `nama`, `jabatan`, `tmt`, `tgl_keluar`, `alasan`, `file`) VALUES
('19580818.198404.1.002', 'Priyo Sudarsono', 'Apoteker', '2021-01-04', '2021-01-18', 'asdasd', 'berkaskaryawanberhenti-19580818.198404.1.002.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(10) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `gender` enum('l','p') NOT NULL,
  `agama` varchar(50) NOT NULL,
  `kebangsaan` varchar(50) NOT NULL,
  `jumlah_keluarga` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `sk_terakhir` varchar(255) DEFAULT NULL,
  `pangkat` varchar(100) NOT NULL,
  `tmt_cpns` date DEFAULT NULL,
  `tmt` date DEFAULT NULL,
  `jenis_pegawai` varchar(100) NOT NULL,
  `tmt_capeg` date NOT NULL,
  `status_pegawai` varchar(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `gaji_pokok` int(10) NOT NULL,
  `npwp` varchar(50) NOT NULL,
  `rt` varchar(20) NOT NULL,
  `rw` varchar(10) NOT NULL,
  `desa` varchar(50) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kabupaten` varchar(50) NOT NULL,
  `kodepos` int(20) NOT NULL,
  `wa` varchar(15) NOT NULL,
  `nik` varchar(50) NOT NULL,
  `str` varchar(100) NOT NULL DEFAULT '0',
  `sip` varchar(50) NOT NULL DEFAULT '0',
  `unit` int(11) DEFAULT NULL,
  `bpjs` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nip`, `nama`, `tempat_lahir`, `tgl_lahir`, `gender`, `agama`, `kebangsaan`, `jumlah_keluarga`, `alamat`, `sk_terakhir`, `pangkat`, `tmt_cpns`, `tmt`, `jenis_pegawai`, `tmt_capeg`, `status_pegawai`, `jabatan`, `gaji_pokok`, `npwp`, `rt`, `rw`, `desa`, `kecamatan`, `kabupaten`, `kodepos`, `wa`, `nik`, `str`, `sip`, `unit`, `bpjs`) VALUES
(21, '123881293.123.123.1.1213', 'Nabil', 'Samarinda', '2020-02-02', 'l', 'Islam', 'WNI', 3, 'ahim 5 RT. 10 RW.  2 KODE POS. 75118', '123881293.123.123.1.1213_sk_terakhir_160966429801873.pdf', 'Penata Muda / IIIa', '2021-02-10', '2021-02-19', 'Pegawai Negeri Sipil', '0000-00-00', 'Aktif', 'Kepala UPTD Laboratorium Lingkungan', 20000000, '19389021.123213-123213.12', '10', '2', 'Sempaja', 'Sempaja', 'Samarinda', 75118, '0981821828123', '12129812903891', '12038901283', '102938sda0a9813klj', 1612286730, '82138232138907'),
(23, '1827389123.1232.13.91230', 'Muiz', '101829782973', '1972-01-04', 'l', 'Islam', 'WNI', 3, 'LOA BAKUNG RT. 5 RW.  3 KODE POS. 75126', '', 'Pembina Utama Madya / IVd', '0000-00-00', '0000-00-00', 'PPTH', '0000-00-00', 'Aktif', 'Kepala UPTD Laboratorium Lingkungan', 150000, '127389721838.123.3123-123.123', '5', '3', 'LOA BAKUNG', 'LOA BAKUNG', 'SAMARINDA', 75126, '1273678283', '9213678238218', '', '', 1612286730, '8123786848274'),
(20, '19580818.198404.1.002', 'Priyo Sudarsono', 'samarinda', '2019-04-03', 'l', 'Islam', 'WNI', 5, 'ahim 5 RT. 10 RW.  2 KODE POS. 75118', '19580818.198404.1.002_sk_terakhir_bayar ukt.pdf', 'Pengatur Muda / IIa', '2021-01-04', '2021-01-04', 'honorer', '0000-00-00', 'Aktif', 'Apoteker', 30000000, '01.855.081.4-412.000', '10', '2', 'Sempaja', 'Sempaja', 'Samarinda', 75118, '0813219832134', '195808181984041002', '195808181984041002', 'sip-195808181984041002', NULL, NULL),
(6, '197009261997031007', 'Eko Kurniawan, S.Sos, M.SI', 'Sungailiat', '1970-09-26', 'l', 'Islam', 'WNI', 4, 'LAN DUYUNG VI NO 192 RT. 06 RW. 01 KODE POS. 33215', '197009261997031007_sk_terakhir_coba2.pdf', 'Pengatur Muda / IIa', '2012-10-01', '0000-00-00', 'Pegawai Negeri Sipil', '0000-00-00', 'Aktif', 'Kepala Dinas', 6000000, '34343434', '006', '001', 'Karya Makmur', 'Pemali', 'Bangka', 0, '081316173030', '123213898123', '12372817389', 'askhdjkh213hj12323', NULL, NULL),
(10, '197405142007011031', 'Yurismansyah, ST, MM.', 'Palembang', '1974-05-14', 'l', 'Islam', 'WNI', 5, 'BATIN TIKAL GG. ENGGANO II NO.35 RT. 02 RW. - KODE POS', 'Kenaikan Gaji Berkala', 'Penata Tk I / IIId', '2017-04-01', '0000-00-00', 'Pegawai Negeri Sipil', '2007-01-01', 'Aktif', 'Kepala Bidang Tata Lingkungan', 2597800, '	14.646.387.2.315.000', '02', '03', 'Sungailiat', 'Bangka', 'Bangka', 0, '081373184720', '', '', '0', NULL, NULL),
(7, '197412042005012002', 'Dora Wardani, ST', 'Pangkalpinang', '1974-12-04', 'p', 'Islam', 'WNI', 2, 'M. SHALEH ZAINUDDIN RT. 03 RW. 01 KODE POS. 33118', 'Kenaikan Pengkat', 'Pembina / IVa', '2015-04-01', '0000-00-00', 'Pegawai Negeri Sipil', '2005-01-01', 'Aktif', 'sektretaris', 3602400, '07.431.497.2-304.000', '03', '01', '33118', 'Gabek', 'Pangkalpinang', 0, '081272895811', '', '', '0', NULL, NULL),
(13, '197807132002121006', 'Marison Lubis, ST', 'Jebus', '1978-07-13', 'l', 'Kristen', 'WNI', 3, 'KAMPUK SAMEK SAMPUR RT. 03 RW. 01 KODE POS. 33302', 'Kenaikan Pengkat', 'Penata / IIIc', '2019-04-01', '0000-00-00', 'Pegawai Negeri Sipil', '2002-12-01', 'Aktif', 'Kasi Pemeliharaan Lingkungan Hidup dan Pengelolaan Sampah', 3273200, '07.433.352.7-304.000', '03', '01', 'Air Itam', 'Bukit Intan', 'Pangkalpinang', 0, '08127173540', '', '', '0', NULL, NULL),
(8, '197910072003121001', 'Mega Oktarian, S.SI, M.Eng', 'Toboali', '1979-10-07', 'l', 'Islam', 'WNI', 4, 'JALAN MERANTI NO.226 RT. 04 RW. 02 KODE POS. -', 'Kenaikan Gaji Berkala', 'Pembina / IVa', '2020-04-01', '0000-00-00', 'Pegawai Negeri Sipil', '2003-12-01', 'Aktif', 'Kepala Bidang PLHPSPKLH', 3350600, '084547470304000', '04', '02', 'Gabek I', 'Gabek', 'Pangkalpinang', 0, '085267483807', '', '', '0', NULL, NULL),
(12, '198304192010012013 ', 'Afriza Farnevi, SH, MM', 'Muntok', '1983-04-19', 'p', 'Islam', 'WNI', 4, 'PERUMAHAN BUKIT GELASE ASRI JL. TAIB  RT. 22 RW. 08 KODE POS. -', 'Kenaikan Pengkat', 'Penata Tk I / IIId', '2018-04-01', '0000-00-00', 'Pegawai Negeri Sipil', '2010-01-01', 'Aktif', 'Kasubbag Umum', 3149100, '15.590.544.1-315.000', '22', '08', 'Dul', 'Bukit Intan', 'Bangka Tengah', 0, '082185615000', '', '', '0', NULL, NULL),
(14, '198505042009032008', 'Fianda Revina WidyastutiI, SKM, M.Si', 'Lampur', '1985-05-04', 'p', 'Islam', 'WNI', 5, 'MELATI GG. DAHLIA VII NO.470 RT. 03 RW. 001 KODE POS. 33123', 'Kenaikan Pengkat', 'Pengatur Muda / IIa', '2018-10-01', '0000-00-00', 'Pegawai Negeri Sipil', '2009-03-01', 'Aktif', 'Apoteker', 3021300, '	79.030.385.3-304.000', '003', '001', 'Bukit Merapin', 'Gerunggang', 'Pangkalpinang', 0, '08122573163', '827389127938', '1238721893721897', '2147483647', NULL, NULL),
(18, '981293821378', 'Syadan Asandy Nugraha', 'Babulu Darat', '2018-10-18', 'l', 'Islam', 'WNI', 2, 'ahim 5 RT. 10 RW.  2 KODE POS. 75117', '981293821378_sk_terakhir_160966429801873.pdf', 'Pengatur Muda / IIa', '2021-01-12', '2021-01-12', 'Pegawai Negeri Sipil', '2021-01-12', 'Aktif', 'Dokter', 30000000, '8217387218937', '10', '2', 'Sempaja', 'Sempaja', 'Samarinda', 75117, '0981821828', '2083921391', '1928398123789', '2147483647', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pemberitahuan`
--

CREATE TABLE `pemberitahuan` (
  `nip` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  `status_gaji` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `id` int(11) NOT NULL,
  `kualifikasi` varchar(255) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`id`, `kualifikasi`, `keterangan`) VALUES
(1, 'Dokter Umum', '-'),
(2, 'Dokter PPDS *)', '-'),
(3, 'Dokter Spes Bedah', '-'),
(4, 'Dokter Spes Penyakit Dalam', '-'),
(5, 'Dokter Spes Kes. Anak', '-'),
(6, 'Dokter Spes Obgin', '-'),
(7, 'Dokter Spes Radiologi', '-'),
(8, 'Dokter Spes Onkologi Radiasi', '-'),
(9, 'Dokter Spes Kedokteran Nuklir', '-'),
(10, 'Dokter Spes Anesthesi', '-'),
(11, 'Dokter Spes Patologi Klinik', '-'),
(12, 'Dokter Spes Jiwa', '-'),
(13, 'Dokter Spes Mata', '-'),
(14, 'Dokter Spes THT', '-'),
(15, 'Dokter Spes Kulit dan Kelamin', '-'),
(16, 'Dokter Spes Kardiologi', '-'),
(17, 'Dokter Spes Paru', '-'),
(18, 'Dokter Spes Saraf', '-'),
(19, 'Dokter Spes Bedah Saraf', '-'),
(20, 'Dokter Spes Bedah Orthopedi', '-'),
(21, 'Dokter Spes Urologi', '-'),
(22, 'Dokter Spes Patologi Anatomi', '-'),
(23, 'Dokter Spes Patologi Forensik', '-'),
(24, 'Dokter Spes Rehabilitasi Medik', '-'),
(25, 'Dokter Spes Bedah Plastik', '-'),
(26, 'Dokter Spes Ked. Olah Raga', '-'),
(27, 'Dokter Spes Mikrobiologi Klinik', '-'),
(28, 'Dokter Spes Parasitologi Klinik', '-'),
(29, 'Dokter Spes Gizi Medik', '-'),
(30, 'Dokter Spes Farma Klinik', '-'),
(31, 'Dokter Spes Lainnya', '-'),
(32, 'Dokter Sub Spesialis Lainnya', '-'),
(33, 'Dokter Gizi', '-'),
(34, 'Dokter Gigi Spesialis', '-'),
(35, 'Dokter/Dokter Gigi MHA/MARS **)', '-'),
(36, 'Dokter/Dokter Gigi S2/S3 Kes Masy **)', '-'),
(37, 'S3 (Dokter Konsultan) ***)', '-'),
(38, 'S3 Keperawatan', '-'),
(39, 'S2 Keperawatan', '-'),
(40, 'S1 Keperawatan', '-'),
(41, 'D4 Keperawatan', '-'),
(42, 'Perawat Vokasional', '-'),
(43, 'Perawat Spesialis', '-'),
(44, 'Pembantu Keperawatan', '-'),
(45, 'S3 Kebidanan', '-'),
(46, 'S2 Kebidanan', '-'),
(47, 'S1 Kebidanan', '-'),
(48, 'D3 Kebidanan', '-'),
(49, 'Tenaga Keperawatan Lainnya', '-'),
(50, 'S3 Farmasi / Apoteker', '-'),
(51, 'S2 Farmasi / Apoteker', '-'),
(52, 'Apoteker', '-'),
(53, 'S1 Farmasi / Farmakologi Kimia', '-'),
(54, 'AKAFARMA *)', '-'),
(55, 'Analisis Farmasi', '-'),
(56, 'Asisten Apoteker / SMF', '-'),
(57, 'ST Lab Kimia Farmasi', '-'),
(58, 'Tenaga Kefarmasian Lainnya', '-'),
(59, 'S3 Kesehatan Masyarakat', '-'),
(60, 'S3 Epidemiologi', '-'),
(61, 'S3 Psikologi', '-'),
(62, 'S2 Kesehatan Masyarakat', '-'),
(63, 'S2 Epidemiologi', '-'),
(64, 'S2 Biomedik', '-'),
(65, 'S2 Psikologi', '-'),
(66, 'S1 Kesehatan Masyarakat', '-'),
(67, 'S1 Psikologi', '-'),
(68, 'D3 Kesehatan Masyarakat', '-'),
(69, 'D3 Sanitarian', '-'),
(70, 'D1 Sanitarian', '-'),
(71, 'Tenaga Kesehatan Masyarakat Lainnya', '-'),
(72, 'S3 Gizi / Dietisien', '-'),
(73, 'S2 Gizi / Dietisien', '-'),
(74, 'S1 Gizi / Dietisien', '-'),
(75, 'D4 Gizi / Dietisien', '-'),
(76, 'Akademi / D3 Gizi / Dietisien', '-'),
(77, 'D1 Gizi / Dietisien', '-'),
(78, 'Tenaga Gizi Lainnya', '-'),
(79, 'S1 Fisio Terapis', '-'),
(80, 'D3 Fisio Terapis', '-'),
(81, 'D3 Okupasi Terapis', '-'),
(82, 'D3 Terapi Wicara', '-'),
(83, 'D3 Orthopedi', '-'),
(84, 'D3 Akupuntur', '-'),
(85, 'Tenaga Keterapian Fisik Lainnya', '-'),
(86, 'S3 Opto Elektronika & Apl Laser', '-'),
(87, 'S2 Opto Elektronika & Apl Laser', '-'),
(88, 'Radiografer', '-'),
(89, 'Radioterapis (Non Dokter)', '-'),
(90, 'D4 Fisika Medik', '-'),
(91, 'D3 Teknik Gigi', '-'),
(92, 'D3 Teknik Radiologi & Radioterapi', '-'),
(93, 'D3 Refraksionis Optisien', '-'),
(94, 'D3 Perekam Medis', '-'),
(95, 'D3 Teknik Elektromedik', '-'),
(96, 'D3 Analis Kesehatan', '-'),
(97, 'D3 Informasi Kesehatan', '-'),
(98, 'D3 Kardiovaskular', '-'),
(99, 'D3 Orthotik Prostetik', '-'),
(100, 'D1 Teknik Tranfusi', '-'),
(101, 'Teknik Gigi', '-'),
(102, 'Tenaga IT dengan Teknologi Nano', '-'),
(103, 'Teknisi Patologi Anatomi', '-'),
(104, 'Teknisi Kardiovaskuler', '-'),
(105, 'Teknisi Elektromedis', '-'),
(106, 'Akupuntur Terapi', '-'),
(107, 'Analis Kesehatan', '-'),
(108, 'Tenaga Keterapian Fisik Lainnya', '-'),
(109, 'S3 Biologi', '-'),
(110, 'S3 Kimia', '-'),
(111, 'S3 Ekonomi / Akuntansi', '-'),
(112, 'S3 Administrasi', '-'),
(113, 'S3 Hukum', '-'),
(114, 'S3 Teknik', '-'),
(115, 'S3 Kes. Sosial', '-'),
(116, 'S3 Fisika', '-'),
(117, 'S3 Komputer', '-'),
(118, 'S3 Statistik', '-'),
(119, 'Doktoral Lainnya (S3)', '-'),
(120, 'S2 Biologi', '-'),
(121, 'S2 Kimia', '-'),
(122, 'S2 Ekonomi / Akuntansi', '-'),
(123, 'S2 Administrasi', '-'),
(124, 'S2 Hukum', '-'),
(125, 'S2 Teknik', '-'),
(126, 'S2 Kes. Sosial', '-'),
(127, 'S2 Fisika', '-'),
(128, 'S2 Komputer', '-'),
(129, 'S2 Statistik', '-'),
(130, 'S2 Administrasi Kes. Masy', '-'),
(131, 'Pasca Sarjana Lainnya (S2)', '-'),
(132, 'Sarjana Biologi', '-'),
(133, 'Sarjana Kimia', '-'),
(134, 'Sarjana Ekonomi / Akuntansi', '-'),
(135, 'Sarjana Administrasi', '-'),
(136, 'Sarjana Hukum', '-'),
(137, 'Sarjana Teknik', '-'),
(138, 'Sarjana Kes. Sosial', '-'),
(139, 'Sarjana Fisika', '-'),
(140, 'Sarjana Komputer', '-'),
(141, 'Sarjana Statistik', '-'),
(142, 'Sarjana Lainnya (S1)', '-'),
(143, 'Sarjana Muda Biologi', '-'),
(144, 'Sarjana Muda Kimia', '-'),
(145, 'Sarjana Muda Ekonomi / Akuntansi', '-'),
(146, 'Sarjana Muda Administrasi', '-'),
(147, 'Sarjana Muda Hukum', '-'),
(148, 'Sarjana Muda Teknik', '-'),
(149, 'Sarjana Muda Kes. Sosial', '-'),
(150, 'Sarjana Muda Sekretaris', '-'),
(151, 'Sarjana Muda Komputer', '-'),
(152, 'Sarjana Muda Statistik', '-'),
(153, 'Sarjana Muda / D3 Lainnya', '-'),
(154, 'SMA / SMU', '-'),
(155, 'SMEA', '-'),
(156, 'STM', '-'),
(157, 'SMKK', '-'),
(158, 'SPSA', '-'),
(159, 'SMTP', '-'),
(160, 'SD Kebawah', '-'),
(161, 'SMTA Lainnya', '-');

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE `profil` (
  `id` int(1) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `instansi` text NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `logo` varchar(100) NOT NULL,
  `bg` varchar(100) NOT NULL,
  `fb` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `ig` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id`, `nama`, `instansi`, `provinsi`, `kota`, `alamat`, `logo`, `bg`, `fb`, `twitter`, `ig`) VALUES
(1, 'SIMPEG', '', 'Kalimantan Timur', 'Samarinda', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `rekap_absensi`
--

CREATE TABLE `rekap_absensi` (
  `id_rekap` bigint(100) NOT NULL,
  `tgl` date NOT NULL,
  `nip` varchar(50) NOT NULL,
  `i` int(10) NOT NULL,
  `s` int(10) NOT NULL,
  `c` int(10) NOT NULL,
  `tk` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rekap_absensi`
--

INSERT INTO `rekap_absensi` (`id_rekap`, `tgl`, `nip`, `i`, `s`, `c`, `tk`) VALUES
(2, '2019-01-01', '196403261987101001', 1, 0, 0, 0),
(3, '2019-01-01', '9967564568943644234', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sk`
--

CREATE TABLE `sk` (
  `id` int(10) NOT NULL,
  `jenis` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sk`
--

INSERT INTO `sk` (`id`, `jenis`, `keterangan`) VALUES
(1, 'SK WALIKOTA', '-'),
(2, 'SK GUBERNUR', '-'),
(3, 'SK DINAS KESEHATAN KOTA', '-'),
(4, 'SK DIREKTUR', '-'),
(5, 'PERWALI (PERATURAN WALIKOTA)', '-'),
(6, 'PERDIR (PERATURAN DIREKTUR)', '-'),
(7, 'SURAT MASUK', '-'),
(8, 'SURAT KELUAR', '-'),
(9, 'SURAT EDARAN', '-');

-- --------------------------------------------------------

--
-- Table structure for table `s_ijasah`
--

CREATE TABLE `s_ijasah` (
  `id_s_ijasah` bigint(100) NOT NULL,
  `tgl` date NOT NULL,
  `banyaknya` int(10) NOT NULL,
  `tipe` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `nomor_surat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `s_kenaikan`
--

CREATE TABLE `s_kenaikan` (
  `id_s_kenaikan` bigint(100) NOT NULL,
  `tgl` date NOT NULL,
  `d` bigint(100) NOT NULL,
  `m` bigint(20) NOT NULL,
  `y` int(5) NOT NULL,
  `banyaknya` int(10) NOT NULL,
  `tipe` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `nomor_surat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kualifikasi`
--

CREATE TABLE `tb_kualifikasi` (
  `id` int(11) NOT NULL,
  `kualifikasi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_mutasi_pendidikan`
--

CREATE TABLE `tb_mutasi_pendidikan` (
  `id` int(11) NOT NULL,
  `kualifikasi_id` int(11) NOT NULL,
  `mutasi_nip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_mutasi_pendidikan`
--

INSERT INTO `tb_mutasi_pendidikan` (`id`, `kualifikasi_id`, `mutasi_nip`) VALUES
(9, 50, '19580818.198404.1.002'),
(10, 52, '19580818.198404.1.002'),
(11, 53, '19580818.198404.1.002');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pegawai_kualifikasi`
--

CREATE TABLE `tb_pegawai_kualifikasi` (
  `id` int(11) NOT NULL,
  `kualifikasi_id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pegawai_kualifikasi`
--

INSERT INTO `tb_pegawai_kualifikasi` (`id`, `kualifikasi_id`, `pegawai_id`) VALUES
(51, 2, 18),
(52, 3, 18),
(53, 50, 14),
(54, 51, 14),
(55, 52, 14),
(56, 56, 14),
(72, 50, 20),
(73, 52, 20),
(74, 53, 20),
(76, 154, 6),
(77, 157, 6),
(79, 128, 21),
(81, 40, 23);

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `unit`) VALUES
(1612286730, 'UGD'),
(1612286737, 'IGD');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(20) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `password` varchar(225) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `level` varchar(50) NOT NULL,
  `gender` enum('l','p') NOT NULL,
  `foto` varchar(100) NOT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `nip`, `password`, `nama`, `level`, `gender`, `foto`, `status`) VALUES
('coba123', '981293821378', '$2y$05$xpcOJMYaMH2sphHDhANA4OY5So3WG2k6WBKCkGJGyMnGE3atKev32', 'Syadan Asandy Nugraha', 'user', 'l', '', 'Aktif'),
('dora', '19741204 200501 2 002', '$2y$05$G2Fh4et2yDGhSX5bupeYkuRZ/h3Cg7yvkTdbKAsJKVqwwykWdqWA6', 'Dora Wardani', 'admin master', 'p', 'dora_dora.png', 'Aktif'),
('kepala', '197009261997031007', '$2y$05$ngoihVvKQ9/IK7WGPLjNrOcy5ViJsQMcM73HCqrmjb1u/0dQsfZg6', 'kepala', 'admin', 'l', '', 'Aktif'),
('megaoktarian', '197910072003121001', '$2y$05$a0hjCAg7m9axIJzW4WgkYeTUlBeDjqWRik1sSlGbdpBiu/eYMXC7.', 'Mega Oktarian, S.SI, M.Eng', 'user', 'l', 'megaoktarian_Mega Oktarian.jpg', 'Aktif'),
('robialakbar', '199403242015021001', '$2y$05$C2q8dTcpUZWPVcUi.Fs8ue47cNpYbv.sQdaq0b5gJUUXOkrfQxH.u', 'robi al akbar', 'user', 'l', 'robialakbar_Untitled.png', 'Aktif'),
('sandi123', '198304192010012013', '$2y$05$/mykAv/64dZIQz/nY3RVneMRAH/cqfqL2YAyDhmemUK1biFdpNR4.', 'Sandi', 'admin', 'l', 'sandi123_profil.png', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `wa`
--

CREATE TABLE `wa` (
  `id` int(1) NOT NULL,
  `token` varchar(155) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wa`
--

INSERT INTO `wa` (`id`, `token`) VALUES
(1, 'PKNHakeUXpqQVWYv20qCGqcmyRfxdDgk8rzkzafFshf1o7d0y3Q4DWyOwnhSSbBn');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anak`
--
ALTER TABLE `anak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `berkas`
--
ALTER TABLE `berkas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `berkas_sk`
--
ALTER TABLE `berkas_sk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuti`
--
ALTER TABLE `cuti`
  ADD PRIMARY KEY (`id_cuti`);

--
-- Indexes for table `detail_s_ijasah`
--
ALTER TABLE `detail_s_ijasah`
  ADD PRIMARY KEY (`id_detail_s_ijasah`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`golongan`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`50`);

--
-- Indexes for table `jml_hari_rekap`
--
ALTER TABLE `jml_hari_rekap`
  ADD PRIMARY KEY (`id_jml`);

--
-- Indexes for table `keluarga`
--
ALTER TABLE `keluarga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mutasi`
--
ALTER TABLE `mutasi`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rekap_absensi`
--
ALTER TABLE `rekap_absensi`
  ADD PRIMARY KEY (`id_rekap`);

--
-- Indexes for table `sk`
--
ALTER TABLE `sk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_ijasah`
--
ALTER TABLE `s_ijasah`
  ADD PRIMARY KEY (`id_s_ijasah`);

--
-- Indexes for table `s_kenaikan`
--
ALTER TABLE `s_kenaikan`
  ADD PRIMARY KEY (`id_s_kenaikan`);

--
-- Indexes for table `tb_kualifikasi`
--
ALTER TABLE `tb_kualifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_mutasi_pendidikan`
--
ALTER TABLE `tb_mutasi_pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pegawai_kualifikasi`
--
ALTER TABLE `tb_pegawai_kualifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `wa`
--
ALTER TABLE `wa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berkas_sk`
--
ALTER TABLE `berkas_sk`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `detail_s_ijasah`
--
ALTER TABLE `detail_s_ijasah`
  MODIFY `id_detail_s_ijasah` bigint(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `50` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT for table `sk`
--
ALTER TABLE `sk`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_kualifikasi`
--
ALTER TABLE `tb_kualifikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_mutasi_pendidikan`
--
ALTER TABLE `tb_mutasi_pendidikan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_pegawai_kualifikasi`
--
ALTER TABLE `tb_pegawai_kualifikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
